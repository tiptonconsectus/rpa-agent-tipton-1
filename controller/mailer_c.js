const Json2csvParser = require('json2csv').Parser;
const os = require('os');
const fs = require('fs');
const path = require("path");
const config =require("../config/config")
const OauthToken = require("../services/utils/oauth2");
//const Oauth2 = new OauthToken();
const http = require("../services/utils/httprequest");
//const http = new httpRequest();
function Json2Csv(req, res) {

    getData_Server().then((success) => {
        let keys = Object.keys(success.results[0]);
        //  console.log(myCars);
        const json2csvParser = new Json2csvParser({ keys });
        csv = json2csvParser.parse(success.results)
        try {
            // var now = new Date();
            var docx_file = path.join(__dirname, '../maildir/mailerlist.csv');
            // console.log(docx_file);
            fs.writeFileSync(docx_file, csv)
            res.status(200).json({
                status: 200,
                data: "csv imported Successfully"
            }).end();
        } catch (err) {
            // console.log(err)
            res.status(500).json({
                status: "error",
                error: error,
                error_description: "System Error occurrred"
            }).end();
        }
    }).catch((error) => {
        res.status(500).json({
            status: "error",
            error: error,
            error_description: "System Error occurrred"
        }).end();
    })
}
async function execute() {
    let filename = path.join(__dirname, "../action-paths/mail");
    const ActionPath = require(filename);
    let action = new ActionPath(req = {}, res = {});
    var now = new Date();
    var filename = 'newaccount'+now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + "-" + now.getHours() + "-" + now.getMinutes() + "-" + now.getSeconds() + "-" + now.getMilliseconds() ;
    let params = { file: filename }
    let response = await action.execute(params);
    let status = 2; // execution finished
    if (response.status === undefined) {
        status = -1;
    } else if (response.status === 'error') {
        status = -1;
    }
    return response;
}
function getData_Server() {

    return new Promise((resolve, reject) => {
        //     let oauth = new Oauth2("18.188.127.1",8888);
        //   //  http://18.188.127.1:8888/accounts/getmaillist
        //     oauth.getAccessToken(true).then(() => {
      
        let oauth = new OauthToken(config.aws_host, config.aws_port);
        oauth.getAccessToken(true).then(() => {

            // console.log(oauth.token)
            const options = {
                hostname: config.aws_host,
                port:  config.aws_port,
                path: '/accounts/getmaillist',
                method: 'GET',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer 1effb44e-b539-4b9c-91a8-57c17d6a64a3',
                }
            };
            const https = new http();
            https.HttpGet(options).then((data) => {
                // console.log(data)
                resolve(data)
            }).catch((err) => {
            //   console.log(err)
                reject(err)
            })
        }).catch((err) => {
            // console.log(err)
            reject(err)
        })

    })
}
function update_aws(){

}
// function postData_Server(){
//     return  new Promise((resolve, reject) => {
//         let oauth = new Oauth2('18.188.127.1',8888);
//       //  http://18.188.127.1:8888/accounts/getmaillist
//         oauth.getAccessToken(true).then(() => {
//             console.log(oauth.token)
//             const options = {
//                 hostname:  '18.188.127.1',
//                 port: 8888,
//                 path: '/accounts/getmaillist',
//                 method: 'get',
//                 headers: {
//                     'Content-Type': 'application/x-www-form-urlencoded',
//                     'Authorization': 'Bearer ' + oauth.token,
//                 }
//             };
//             const https= new http();
//             https.HttpGet(options).then((data)=>{
//                 console.log(data)
//                resolve(data)
//            }).catch((err)=>{
//                console.log(err)
//                reject(err)
//            })
//         }).catch((err)=>{
//             console.log(err)
//             reject(err)
//         })

//     })
// }

module.exports.Json2Csv = Json2Csv;