const debug = require("debug")("get-action-paths:");
const ActionPathController = require("../manage/action-path-controller");
const actionPathController = new ActionPathController();
module.exports = function (req, res, next) {
    if (req.body.name === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name not found"
        }).end();
    } else if (req.body.name.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name cannot be blank"
        }).end();
    } else {
        const name = req.body.name;
        actionPathController.getActionPathByName(name).then((results)=>{
            debug(results);
            if (results.length === 0){
                res.status(500).json({
                    status: "error",
                    error: "not_found",
                    error_description: "action path not found"
                }).end();        
            }else{
                debug(results[0]);
                actionPathController.getQueue(results[0]).then((results) => {
                    let ret = {
                        status: "OK",
                        data: results
                    }
                    res.status(200).json(ret).end();
                }).catch((error) => {
                    debug(error);
                    res.status(500).json({
                        status: "error",
                        error: error,
                        error_description: "System Error occurrred"
                    }).end();
                })
                        
            }
        }).catch((error)=>{
            debug(error);
            res.status(500).json({
                status: "error",
                error: error,
                error_description: "System Error occurrred"
            }).end();
        });
    }
}