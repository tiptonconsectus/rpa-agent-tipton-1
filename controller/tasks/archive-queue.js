const debug = require("debug")("get-action-paths:");
const ActionPathController = require("../manage/action-path-controller");
const actionPathController = new ActionPathController();
module.exports = function (req, res, next) {
    const name = req.body.name;
    actionPathController.archiveQueue().then((results) => {
        // debug(results);
        let ret = {
            status: "OK",
            data: results
        }
        res.status(200).json(ret).end();
    }).catch((error) => {
        debug(error);
        res.status(500).json({
            status: "error",
            error: error,
            error_description: "System Error occurrred"
        }).end();
    });
}