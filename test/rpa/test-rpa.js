/**
 * Refer to Sikuli Java API 
 * http://doc.sikuli.org/javadoc/index.html
 * 
 * Refer to node-java npm module 
 * https://github.com/joeferner/node-java 
 * 
 */
const ActionPath = require("../../lib/action-path");
class TestRPA extends ActionPath{
    constructor(req, res){
        super(req, res);
    }
    execute(params){
        this.getScreen();
        const cortana = this.find("cortana.png");
        if (cortana){
            this.click(cortana);
            this.type("Notepad");
        }            
    }
}
const test = new TestRPA();
test.execute({
    write: "Hello from RPA",
    filename: "santosh.txt"
})
