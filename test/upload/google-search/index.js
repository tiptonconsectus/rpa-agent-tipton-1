const path = require("path");
ActionPath = require("../../lib/action-path");
class GoogleSearch extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    execute(params) {
        try {
            this.debug(params);
            this.debug("getting screen");
            //click chrome
            if (this.clickImage(path.join(__dirname, "chrome.png"), 0.90)) {
                //chrome icon found
                return {
                    status: "ok",
                    message: "finder typed seearch",
                    data: ret
                }
            } else {
                if (this.clickImage(path.join(__dirname, "finder-icon.png"), 0.90, -18, 0)) {
                    this.wait(0.5);
                    this.type("Chrome");
                }
                return {
                    status: "ok",
                    message: "finder typed seearch",
                    data: "clicked"
                }
            }
        } catch (error) {
            this.debugerror(error);
            return {
                status: "error",
                error: error
            }
        }
    }
}
module.exports = GoogleSearch
