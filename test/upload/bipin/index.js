const path = require("path");
ActionPath = require("../../lib/action-path");
class Shukla extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    execute(params) {
        try {
            console.log(params)
            this.debug(params);

            const x = this.clickImage(path.join(__dirname, "explorer.PNG"));
            this.debug(x);
            if (x) {
                return {
                    status: "ok",
                    data: {
                        params: params,
                        explorer: x,
                        screen: this.screen
                    }
                }
            } else {
                return {
                    status: "error",
                    data: {
                        params: params,
                        explorer: "explorer.PNG not found",
                        screen: this.screen
                    }
                }
            }

        } catch (error) {
            this.debugerror(error);
            return {
                status: "error",
                error: error
            }
        }

    }
}
module.exports = Shukla