let port = process.env.PORT || 9089;
let database = process.env.DATABASE || 'consectus_rpa_agent';
let database_user = process.env.DATABASE_USER || 'consectus_rpa_agent';
let database_password = process.env.DATABASE_PASSWORD || 'ITISSAFE';
let database_host = process.env.DATABASE_HOST || 'localhost';
let database_port = process.env.DATABASE_PORT || "3306";
let action_path_location = process.env.ACTION_PATH_LOCATION || "action-paths";
let agent_temp = process.env.Agent_Temp || "temp";


module.exports = {
    "sopra": "demo",
    'port': port,
    "soprafolder": "sopraerror",
    'secret': 'xyz-abc-def',
    'callback_key': '@1fky!3#zkt-wyst@rxzz!!#$%^&*(eytKEY',
    "aws_host": "localhost",
    "aws_port": port,
    "processInterval": 10000,
    "action_path_location": action_path_location,
    "agent_temp": agent_temp,
    "database": {
        "host": database_host,
        "user": database_user,
        "password": database_password,
        "database": database,
        "port": database_port
    },
    "tables": {
        'settings': "rpa_agent_settings",
        "paths": 'action_paths',
        "queues": 'action_path_queues',
        "queue_archive": "action_path_queue_archive"
    },
    "settings": {
        "name": "Agent 1",
        "id": "1",
        "setAutoWaitTimeout": "1",
        "matchScore": "0.90",
        "adminEmail": "santosh@consectus.com"
    },
    "twilio": {
        "accountsid": "AC2b7f2c142bb658cc8020c0cdc90f230e",
        "authToken": "3aac5b84ef8c216b37396d41452b2b7e"
    },
    "nodemailer": {
        "email": "support@consectus.com",
        "passwd": "C0ns3ctus@",
        "secure": true,
        "smtp": "mail.consectus.com",
        "port": 465
    },
    "template_path": "C:\\Users\\Consectus\\Desktop\\Hello.docx",
    "mailFile_save": "E:\\projects\\rpa-agent\\maildir"
};