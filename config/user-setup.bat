REM Add Firewall rules to allow Agent ports

netsh advfirewall firewall add rule name="RPA Agent Port 8441-8450" dir=in action=allow protocol=TCP localport=8441-8450

REM Copy Server startup batch file for all users in their startup folder

copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent1\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent2\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent3\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent4\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent5\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent6\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent7\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent8\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent9\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
copy "C:\Users\Public\rpa-agent-tipton\config\run-agent.bat" "C:\Users\rpa-agent10\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\run-agent.bat"
