/* create database */
CREATE database IF NOT EXISTS consectus_rpa_agent;
CREATE USER IF NOT EXISTS 'consectus_rpa_agent'@'localhost' IDENTIFIED BY 'c0ns3ctusrp@';
GRANT ALL PRIVILEGES ON consectus_rpa_agent.* TO 'consectus_rpa_agent'@'localhost';
FLUSH PRIVILEGES;
