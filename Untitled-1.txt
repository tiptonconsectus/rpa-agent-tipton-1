server {
    listen 443;
    server_name apptest.thetiptondigital.com;
    client_max_body_size 50M;
    ssl_session_cache shared:SSL:10m;
    keepalive_timeout 30;
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;
    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://127.0.0.1:8888;
    }
    location /estimated/download {
        proxy_pass http://192.168.12.10:443/estimated/download;
    }
}
server {
        listen 80;
        proxy_set_header        X-Real-IP       $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
​
        location / {
                proxy_pass http://127.0.0.1:8888;
        }
