const debug = require("debug")("db:");
const config = require("../config/config");
const dbhelper = require("../model/dbhelper");
class SettingModel {
    constructor(model) {
        this.id = (model === undefined) ? null : model.id;
        this.name = (model === undefined) ? null : model.name;
        this.value = (model === undefined) ? null : model.value;
    }
    getAllSettings() {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM ??";
            let inserts = [config.tables.settings]
            query = dbhelper.mysql.format(query, inserts)
            debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                results = JSON.parse(JSON.stringify(results))
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    getSetting(name) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM ?? WHERE ??=? LIMIT 1";
            let inserts = [config.tables.settings, 'name', name]
            query = dbhelper.mysql.format(query, inserts)
            debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                // results = JSON.parse(JSON.stringify(results))
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    saveSetting(setting) {
        let self = this;
        return new Promise((resolve, reject) => {
            let sql = null;
            if (setting.id === undefined || path.id === null) {
                //insert
                sql = dbhelper.mysql.format(`INSERT INTO ?? (??, ??) VALUES (?,?)`,
                    [config.tables.settings, 'name', 'value', setting.name, setting.value]);
            } else {
                //update
                sql = dbhelper.mysql.format(`UPDATE ?? SET ??=? WHERE ??=?`,
                    [config.tables.settings, 'name', setting.name, 'value', setting.value]);
            }
            debug(sql);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                results = JSON.parse(JSON.stringify(results))
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    updateSetting(name, value) {
        // console.log("name inside")
        // console.log(name)
        // console.log(value)
        return new Promise((resolve, reject) => {
            let query = "UPDATE ?? SET ??=? "
            query += "  WHERE name='" + name + "'";
            let inserts = [config.tables.settings, "value", value];
            query = dbhelper.mysql.format(query, inserts)

            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });

    }
    createSetting(name, value) {
        return new Promise((resolve, reject) => {
            let query = "INSERT INTO ?? (??, ??) VALUES (?,?)"
            let inserts = [config.tables.settings, "name", "value", name, value];
            query = dbhelper.mysql.format(query, inserts)

            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });

    }
}
module.exports = SettingModel;