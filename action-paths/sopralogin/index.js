const path = require("path");
ActionPath = require("../../lib/action-path");
//const config = require("../../config/config.js")
let sopra = "demo1";
let trytimes = 7;
let waitforimage = 60; //sec
let waitinloop = 10;
let Sopra_password = 'Post1preing';
class sopraLogin extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }

    ensureSopraScreen() {
	 let nwerr4;
            let nwerr5;
            let nwerr7;
        // let sopra = this.wait(path.join(__dirname, "./login/sporascreen.png"));
        const data1 = this.runShellSync("tasklist /V /FO CSV /NH /FI \"IMAGENAME eq iexplore.exe\" /FI \"USERNAME eq %username%\" ");
        if (data1.indexOf("No tasks are running") === -1) {

           
            let menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.85, 0, 0), 10);
            this.click(menu);
            this.type("ACHC", menu);
            let menucheck = this.wait(this.getPattern(path.join(__dirname, "./login/achcinmenu.png"), 0.90, 80, 0), 10);
            if (menucheck) {
                this.click(menucheck)
                for (let x = 0; x <= 5; x++) {
                    this.keys("\b")
                }
                menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.85, 0, 0), 10);// this.wait(path.join(__dirname, "./login/menuprompt.png"));
                if (menu) {
                    return 1;
                } else {
                    return this.closeExplorer();
                }
            }
            else {
                let nwerr1 = this.wait(this.getPattern(path.join(__dirname, "./nw/nwerr1.png"), 0.90, 0, 0));
                if (!nwerr1) nwerr4 = this.wait(this.getPattern(path.join(__dirname, "./nw/nwerr4.png"), 0.90, 0, 0));

                if (!nwerr4) nwerr5 = this.wait(this.getPattern(path.join(__dirname, "./nw/nwerr5.png"), 0.90, 0, 0));
                if (!nwerr5) nwerr7 = this.wait(this.getPattern(path.join(__dirname, "./nw/nwerr7.png"), 0.90, 0, 0));
                if (nwerr1 || nwerr4 || nwerr5 || nwerr7) {
                    this.closeExplorer();
                    return 2;
                } else {
                    return this.closeExplorer();
                }
            }
        } else {
            return this.closeExplorer();
        }


    }
    closeExplorer() {
        const data = this.runShellSync("tasklist /V /FO CSV /NH /FI \"IMAGENAME eq iexplore.exe\" /FI \"USERNAME eq %username%\" ");
        if (data.indexOf("No tasks are running") > 0) {
            //start the explorer
            
            this.runShell("\"C:\\Program Files\\internet explorer\\iexplore.exe\" http://192.168.20.116:8888/forms/start/index.html?env=c5anon");
            
            return this.ensureSopraLoginScreen();

        } else {
            // console.log("already login false")
            // console.log("close sopra");
            this.hardClose("iexplore.exe");
            // console.log("ran sopra");
            this.runShell("\"C:\\Program Files\\internet explorer\\iexplore.exe\" http://192.168.20.116:8888/forms/start/index.html?env=c5anon");
            return this.ensureSopraLoginScreen();

        }
    }
    ensureSopraLoginScreen() {
        let indexvalue = 0;
        let loginscreencheck;
       
        while (true) {
         //    if (indexvalue === 1 || indexvalue === 5 || indexvalue === 13 || indexvalue === 20 || indexvalue === 30) {
         //       try {
        //            this.appFocus('C:\\Program Files\\internet explorer\\iexplore.exe');
        //        } catch (err) {
        //
        //        }
       //     }
            indexvalue++;
            if (indexvalue >= 35) {
                return -2;
            }
            loginscreencheck = this.wait(this.getPattern(path.join(__dirname, "./login/loginscreen.png"), 0.85, 0, 0), 10);
            if (loginscreencheck) {
                this.keys("u\"\ue023\"", "u\"\ue000\"");
                let menu;
                let username = this.wait(this.getPattern(path.join(__dirname, "./login/username.png"), 0.85, 0, 0), 10);//  this.wait(path.join(__dirname, "./login/username.png"));
                this.type('consectus', username);
                let password = this.wait(this.getPattern(path.join(__dirname, "./login/password.png"), 0.85, 0, 0));//  this.wait(path.join(__dirname, "./login/password.png"));
                // this.click(password)
                this.type(Sopra_password, password);
                let passwrdcheck = this.wait(this.getPattern(path.join(__dirname, "./login/loginpass" + Sopra_password.length + ".png"), 0.85, 0, 0), 60);
                if (passwrdcheck) {
                    let login = this.wait(this.getPattern(path.join(__dirname, "./login/login.png"), 0.85, 0, 0));//  this.wait(path.join(__dirname, "./login/login.png"));
                    if (login) {
                        for (let i = 0; i < 7; i++) {
                            let loginclick = this.click(login);
                            menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.80, 0, 0), 60);

                            if (loginclick || menu) { break; }
                            if (i == 6) { return -1; }
                        }
                        this.keys("u\"\ue023\"", "u\"\ue000\"");
                        this.highlight(menu);
                        return 1;
                    } else {
                        return -1;
                    }
                } else {
                    return -1;
                }
            } else {
                this.clickImage(path.join(__dirname, "./login/accept.png"));
                this.clickImage(path.join(__dirname, "./login/run.png"));
                // console.log("dont--------->", indexvalue)
                this.clickImage(path.join(__dirname, "./login/dontshow.png"));
                this.clickImage(path.join(__dirname, "./login/run.png"));
            }

        }

    }
}

module.exports = sopraLogin;

