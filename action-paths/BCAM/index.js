const path = require("path");
ActionPath = require("../../lib/action-path");
let sopraLogin = require("../sopralogin/index");
let loginsopra = new sopraLogin();
let trytimes = 7;

const config = require("../../config/config")
// config.sopra ="demo";
class BACM extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    execute(params, callback = null) {
        try {


            //accountsortcode
            // accountholdername
            //nominatedbankno
            // let params = {
            //     accountsortcode: '999999',
            //     accno: "98003430",
            //     nominatedbankno: "29250976",
            //     accountholdername: "test1",
            // }
            params.datefrom = params.datefrom ? params.datefrom : "25-FEB-2019"
            let menubcm;
            let menu = this.wait(this.getPattern(path.join(__dirname, "./menuprompt.png"), 0.90, 0, 0), 60);
            this.type('BACM', menu);
            this.keys("\n");
            this.keys("\n");
            for (let i = 0; i < trytimes; i++) {
                let acc = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bcaacc.png"), 0.90, 0, 0), 30);
                let acctype = this.type(params.accno, acc);

                if (acctype) break;
                if (trytimes - 1 === i) throw "In BACM Image:- ./accsearch/bcaacc.png, Developer:-  acc-" + acc + " tries time:-" + i;
            }
            let createlink;
            let go;
            let search;
            let alert;
            let sortcode;
            for (let i = 0; i < trytimes; i++) {
                if (!alert && !go) {
                    search = this.wait(this.getPattern(path.join(__dirname, "./accsearch/search.png"), 0.90, 0, 0), 10);
                    this.click(search);
                }
                go = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bcamgo.png"), 0.90, 0, 0), 5);
                if (!go) alert = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bcamalert.png"), 0.90, 100, 55), 5);
                if (alert) {
                    let clickalt = this.click(alert);

                    if (clickalt) {
                        throw "account does not exists"
                    }
                }
                if (go) {
                    this.click(go);
                    createlink = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bcamcreatelink.png"), 0.90, -80, -3), 5);
                    if (createlink) {
                        break;
                    }
                }
                if (trytimes - 1 === i) throw "In BACM Image:- ./accsearch/bcamcreatelink.png, ./accsearch/bcamgo.png, ./accsearch/bcamalert.png, Developer:-  go-" + go + " alert " + alert + " createlink " + createlink + " tries time:-" + i;
            }

            for (let i = 0; i < trytimes; i++) {
                createlink = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bcamcreatelink.png"), 0.90, -80, -3), 5);
                this.click(createlink);
                sortcode = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmsortcode.png"), 0.90, 15, 0), 5);
                if (sortcode) {
                    break;
                }
                if (trytimes - 1 === i) throw "In BACM Image:- ./accsearch/bacmsortcode.png, ./accsearch/bcamcreatelink.png , Developer:-  sortcode-" + sortcode + " createlink-" + createlink + " tries time:-" + i;
            }
            this.type(params.accountsortcode, sortcode);
            this.keys("\t");
            let newaccalert = this.wait(this.getPattern(path.join(__dirname, "./accsearch/newaccalert.png"), 0.85, 20, 0), 7);
            let newaccalert1;
            let accno;
            let bacmclose;
            // let bacmextrabtn;
            let bacmokcancel;
            if (newaccalert) {

                for (let i = 0; i < trytimes; i++) {
                    newaccalert1 = this.wait(this.getPattern(path.join(__dirname, "./accsearch/newaccalert.png"), 0.90, 20, 0));
                    this.click(newaccalert1);
                    bacmokcancel = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmokcancel.png"), 0.90, 43, 0), 3);
                    if (bacmokcancel) break;
                    if (trytimes - 1 === i) throw "In BACM Image:- ./accsearch/bacmaccno.png , Developer:-  bacmokcancel-" + bacmokcancel + " tries time:-" + i;
                }
                for (let i = 0; i < trytimes; i++) {
                    bacmokcancel = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmokcancel.png"), 0.90, 43, 0), 3);
                    this.click(bacmokcancel);
                    bacmclose = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmclose.png"), 0.90, 0, 0), 3);
                    if (bacmclose) break;
                    if (trytimes - 1 === i) throw "In BACM Image:- ./accsearch/bacmclose.png, ./accsearch/newaccalert.png , Developer:-  bacmokcancel-" + bacmokcancel + " bacmclose-" + bacmclose + " tries time:-" + i;
                }
                for (let i = 0; i < trytimes; i++) {

                    bacmclose = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmclose.png"), 0.90, 0, 0), 3);
                    this.click(bacmclose);
                    menubcm = this.wait(this.getPattern(path.join(__dirname, "./accsearch/menubcm.png"), 0.90, 56, 0), 20);

                    if (menubcm) {
                        for (let i = 0; i < 6; i++) {
                            this.keys("\b");
                        }
                        break;
                    }
                    if (trytimes - 1 === i) throw "failed to find menu  with bacm";
                }
                params.sopraerror = "failed to add";
                return params.nba = false;

            } else {
                let accname;
                let okclick;
                let accountholdername;
                let bacmaccno1 = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmaccno1.png"), 0.90, 0, 0), 5);
                this.type(params.nominatedbankno, bacmaccno1);
                this.keys("\t");
                let bacmalredys = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmalredys.png"), 0.90, 0, 0), 5);

                if (bacmalredys) {
                    for (let i = 0; i < trytimes; i++) {
                        let bacmalredys1 = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmalredys.png"), 0.90, 0, 0), 5);
                        this.click(bacmalredys1);
                        bacmokcancel = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmokcancel.png"), 0.90, 43, 0), 3);
                        if (bacmokcancel) break;
                        if (trytimes - 1 === i) throw "In BACM Image:-./accsearch/bacmokcancel.png , Developer:-  bacmokcancel-" + bacmokcancel + " tries time:-" + i;

                    }
                    for (let i = 0; i < trytimes; i++) {
                        bacmokcancel = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmokcancel.png"), 0.90, 43, 0), 3);
                        this.click(bacmokcancel);
                        bacmclose = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmclose.png"), 0.90, 0, 0), 3);
                        if (bacmclose) break;
                        if (trytimes - 1 === i) throw "In BACM Image:- ./accsearch/bacmclose.png, ./accsearch/newaccalert.png , Developer:-  bacmokcancel-" + bacmokcancel + " bacmclose-" + bacmclose + " tries time:-" + i;
                    }
                    for (let i = 0; i < trytimes; i++) {
                        bacmclose = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmclose.png"), 0.90, 0, 0), 3);
                        this.click(bacmclose);
                        menubcm = this.wait(this.getPattern(path.join(__dirname, "./accsearch/menubcm.png"), 0.90, 56, 0), 20);
                        if (menubcm) {
                            for (let i = 0; i < 6; i++) {
                                this.keys("\b");
                            }
                            break;
                        }
                        if (trytimes - 1 === i) throw " failed to find menu  with bacm";
                    }

                    params.sopraerror = "account added already";
                    return params.nba = false;
                    // return params.nba = " account added already"

                }
                newaccalert1 = this.wait(this.getPattern(path.join(__dirname, "./accsearch/newaccalert.png"), 0.90, -22, 0), 7);
                if (newaccalert1) {
                    for (let i = 0; i < trytimes; i++) {
                        newaccalert1 = this.wait(this.getPattern(path.join(__dirname, "./accsearch/newaccalert.png"), 0.90, -22, 0));
                        this.click(newaccalert1);
                        accname = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmaccname.png"), 0.90, 0, 0), 3);
                        if (accname) break;
                        if (trytimes - 1 === i) throw "In BACM Image:-./accsearch/newaccalert.png, ./accsearch/bacmaccname.png , Developer:-  newaccalert1-" + newaccalert1 + " accname-" + accname + " tries time:-" + i;
                    }
                    this.type(params.accountholdername, accname);
                    let datefrom = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmdatefrom.png"), 0.90, 0, 0), 60);
                    this.type(params.datefrom, datefrom);
                    this.keys("\t");

                    let guarantee;
                    let guaranteecheck;
                    let okclickerr;
                    for (let i = 0; i < trytimes; i++) {
                        if (!okclickerr && !guaranteecheck) {
                            guarantee = this.wait(this.getPattern(path.join(__dirname, "./accsearch/guarantee.png"), 0.90, -57, -3));
                            this.click(guarantee);
                        }
                        guaranteecheck = this.wait(this.getPattern(path.join(__dirname, "./accsearch/guaranteecheck.png"), 0.90, 0, 0));
                        if (!guaranteecheck) okclickerr = this.wait(this.getPattern(path.join(__dirname, "./accsearch/okclickerr.png"), 0.90, 0, 0));

                        if (okclickerr) {
                            this.click(okclickerr);
                        }
                        else if (guaranteecheck) {
                            this.type('1');
                            break;
                        } else if (!guarantee) {
                            break;
                        }
                    }
                    for (let i = 0; i < trytimes; i++) {
                        okclick = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bcamokclick.png"), 0.90, -40, 0), 60);
                        this.click(okclick);
                        bacmclose = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmclose.png"), 0.90, 0, 0), 3);
                        if (bacmclose) break;
                        if (trytimes - 1 === i) throw "In BACM Image:-./accsearch/bcamokclick.png, ./accsearch/menubcm.png, Developer:-  okclick-" + okclick + " menubcm-" + menubcm + " tries time:-" + i;
                    }
                    for (let i = 0; i < trytimes; i++) {

                        bacmclose = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmclose.png"), 0.90, 0, 0), 3);
                        this.click(bacmclose);
                        menubcm = this.wait(this.getPattern(path.join(__dirname, "./accsearch/menubcm.png"), 0.90, 56, 0), 20);
                        if (menubcm) {
                            for (let i = 0; i < 6; i++) {
                                this.keys("\b");
                            }
                            break;
                        }
                        if (trytimes - 1 === i) throw "failed to find menu  with bacm";
                    }

                } else {
                    let effdatefrom1 = this.wait(this.getPattern(path.join(__dirname, "./accsearch/effdatefrom1.png"), 0.90, 0, 0), 10);
                    this.type(params.datefrom, effdatefrom1);
                    this.keys("\t");
                    let guarantee;
                    let guaranteecheck;
                    let okclickerr;
                    for (let i = 0; i < trytimes; i++) {
                        if (!okclickerr && !guaranteecheck) {
                            guarantee = this.wait(this.getPattern(path.join(__dirname, "./accsearch/guarantee.png"), 0.90, -57, -3));
                            this.click(guarantee);
                        }
                        guaranteecheck = this.wait(this.getPattern(path.join(__dirname, "./accsearch/guaranteecheck.png"), 0.90, 0, 0));
                        if (!guaranteecheck) okclickerr = this.wait(this.getPattern(path.join(__dirname, "./accsearch/okclickerr.png"), 0.90, 0, 0));

                        if (okclickerr) {
                            this.click(okclickerr);
                        }
                        else if (guaranteecheck) {
                            this.type('1');
                            break;
                        } else if (!guarantee) {
                            break;
                        }
                    }
                    for (let i = 0; i < trytimes; i++) {
                        okclick = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bcamokclick.png"), 0.90, -40, 0), 60);
                        this.click(okclick);
                        bacmclose = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmclose.png"), 0.90, 0, 0), 3);
                        if (bacmclose) break;
                        if (trytimes - 1 === i) throw "In BACM Image:-./accsearch/bcamokclick.png, ./accsearch/menubcm.png, Developer:-  okclick-" + okclick + " menubcm-" + menubcm + " tries time:-" + i;
                    }
                    for (let i = 0; i < trytimes; i++) {

                        bacmclose = this.wait(this.getPattern(path.join(__dirname, "./accsearch/bacmclose.png"), 0.90, 0, 0), 3);
                        this.click(bacmclose);
                        menubcm = this.wait(this.getPattern(path.join(__dirname, "./accsearch/menubcm.png"), 0.90, 56, 0), 20);
                        if (menubcm) {
                            for (let i = 0; i < 6; i++) {
                                this.keys("\b");
                            }
                            break;
                        }
                        if (trytimes - 1 === i) throw "failed to find menu  with bacm";
                    }

                }


            }


        } catch (error) {
            try {
                let datetime = new Date().getTime();
                params.img = error.replace(/\s/g, '').substr(0, 6) + datetime;
                this.capture(params.img);
            } catch (e) { }
            this.hardClose("iexplore.exe");
            loginsopra.ensureSopraScreen();
            params.sopraerror = JSON.stringify(error);
            return params.nba = false;
        }

    }
}
// const x = new BACM()
// x.execute({});
// process.exit(0);
module.exports = BACM