const fs = require("fs");
const path = require("path");
const ActionPathClass = require("../lib/action-path");
const debug = require("debug")("execute-action-path:");
var ArgumentParser = require('argparse').ArgumentParser;
var parser = new ArgumentParser({
    version: '1.0.0',
    addHelp: true,
    description: 'Zips and uploads the folder containing Sikuli Script files within the folder.',
    epilog: "Please ensure that the folder name provided contains Sikuli recorded python script & associated image files used in the script.\r"
});
parser.addArgument(
    ['-f', '--folder'],
    {
        required: true,
        help: 'folder name'
    }
);
parser.addArgument(
    ['-p', '--params'],
    {

        help: 'folder name'
    }
);
var args = parser.parseArgs();
// console.log(x.fff)
//console.log(args)
function ExecuteCmd(args) {
    if (args.folder === undefined) {
        console.log("Folder name Require")
    } else if (args.params === undefined) {
        console.log("Params Not Found")
    } else {
        const Filename = path.join(__dirname, "./" + args.folder);
      //  console.log(Filename)
        // debug(configFile);
        if (fs.existsSync(Filename)) {
            debug("folder found")
            console.log("loading action path folder")
            //  let  filename = path.join(__dirname, "./", +args.folder);
            const ActionPath = require(Filename);
            action = new ActionPath(req = {}, res = {});
            try {
                var par = {};
                if (args.params !== undefined) par = JSON.parse(JSON.stringify(eval("(" + args.params + ")")))
                let response = action.execute(par);
                console.log("response")
                console.log(response)
            } catch (err) {
                console.log("err")
                console.log(err)
            }
        }
        else {
            console.log("folder not exists Please Provide  valid folder")
        }
    }


}
ExecuteCmd(args);

// node action-paths/test.js -f 'bipin' -p  "{echo:'xx'}"

