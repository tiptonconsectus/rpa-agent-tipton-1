const http = require("http");
const querystring = require("querystring");
class callBackStep {
  constructor(OauthToken = null) {
    this.OauthToken = OauthToken;
    this.isBusy = false;
  }
  sendStep(callbackData, step) {
    return new Promise((resolve, reject) => {
      const postData1 = querystring.stringify({
        step: JSON.stringify(step)
      });
      let oauth = this.OauthToken(callbackData.host, callbackData.port);

      oauth.getAccessToken(false).then(() => {
        const options = {
          hostname: callbackData.host,
          port: callbackData.port,
          path: "/estimated" + callbackData.path,
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + oauth.token,
            'Content-Length': Buffer.byteLength(postData1)
          }
        };
        this.HttpPost(options, postData1).then((Data) => {
          resolve();
        }).catch((err) => {
          resolve();
        })
      }).catch((error) => {
        resolve();
      })
    })
  }
  HttpPost(options, postData) {
    return new Promise((resolve, reject) => {
      const req = http.request(options, (res) => {
        let data = "";
        let error = (res.statusCode !== 200);
        res.setEncoding('utf8');
        res.on('data', (chunk) => {
          data += chunk;
          //use here resolve
        });
        res.on('end', () => {
          try {
            data = JSON.parse(data);

          } catch (e) {
            data = data;
            // console.log(e);
            // reject("error in try method")
          }
          if (error) {
            reject(data)
          } else {
            resolve(data)
          }
          // debug('No more data in response.');
        });
      })
      req.on('error', (e) => {

        // debugerror(`problem with request: ${e.message}`);
        reject(e);
      });
      // write data to request body
      req.write(postData);
      req.end();
    })
  }
}
module.exports = callBackStep