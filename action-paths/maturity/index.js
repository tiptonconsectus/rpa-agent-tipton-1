const path = require("path");
ActionPath = require("../../lib/action-path");
let sopraLogin = require("../sopralogin/index");
let loginsopra = new sopraLogin();
//const config = require("../../config")
let sopra = "demo";
let trytimes = 7;
let waitforimage = 60; //sec
let waitinloop = 10;
class ACATC extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    async execute(params, callback = null) {
        try {
            if(sopra === "demo"){
            let login = loginsopra.ensureSopraScreen();
            if (login === 1) {
                this.ACATC(params);
                return {
                    status: "ok", message: "successfully", data: params
                }
            }
            else if (login === -1) {
                try {
                    let datetime = new Date().getTime();
                    params.img = "login" + datetime;
                    this.capture(params.img);
                } catch (e) { }
                this.hardClose("iexplore.exe");
                params.sopraerror = "login failed"
                return {
                    status: "-1", message: "rpa_agent_failed", data: params
                }
            } else if (login === -2) {
                try {
                    let datetime = new Date().getTime();
                    params.img = "Soprainstancefailed" + datetime;
                    this.capture(params.img);
                } catch (e) { }
                this.hardClose("iexplore.exe");
                params.sopraerror = "Sopra instance is not available"
                return {
                    status: "-1", message: "rpa_agent_failed", data: params
                }
            }
            else {
                try {
                    let datetime = new Date().getTime();
                    params.img = "networkerr" + datetime;
                    this.capture(params.img);
                } catch (e) { }
                this.hardClose("iexplore.exe");
                // loginsopra.ensureSopraScreen();
                params.sopraerror = "newtork error occured";
                return {
                    status: "error",
                    data: params,
                    message: "rpa_agent_failed"
                }
            }
         }else{
            return {
                status: "ok", message: "successfully", data: params
            }
         }
        } catch (error) {
            loginsopra.ensureSopraScreen();
            // console.log(error);
            this.debugerror(error);
            params.sopraerror = JSON.stringify(error);
            params.screen = this.getScreen();
            return {
                status: "error",
                data: params,
                message: "rpa_agent_failed"
            }
        }

    }
    ACATC(params) {
        try {

            let menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.90, 0, 0), 60);
            this.type('ACATC', menu);
            this.keys("\n");
            this.keys("\n");
            let acatcaccno;
            let typeaccno;
            let acatcoverallyes;

            for (let i = 0; i < trytimes; i++) {
                if (!acatcaccno) acatcaccno = this.wait(this.getPattern(path.join(__dirname, "./acatc/acatcaccno.png"), 0.90, 0, 0), 10);
                typeaccno = this.type(params.accno, acatcaccno);
                // acatcaccno = this.wait(this.getPattern(path.join(__dirname, "./acatc/acatcaccno.png"), 0.90, 0, 0));
                this.type("\t");
                let actcfindaccdetail = this.wait(this.getPattern(path.join(__dirname, "./acatc/actcfindaccdetail.png"), 0.90, 0, 0));
                if (!actcfindaccdetail) break
            }
            // this.keys("\b");
            // let accdate = this.wait(this.getPattern(path.join(__dirname, "./acatc/acatcdatechange.png"), 0.90, 130, 0), 5);
            // if (accdate) {
            //     this.type(params.date, accdate);
            // }


            let acatctype;
            let okclickpop;
            for (let i = 0; i < trytimes; i++) {
                if (!acatctype) {
                    acatctype = this.wait(this.getPattern(path.join(__dirname, "./acatc/acatcacctype.png"), 0.90, 30, 0), 10);
                    this.type(params.sortcode, acatctype);
                }
                this.type("\t");
                acatcoverallyes = this.wait(this.getPattern(path.join(__dirname, "./acatc/acatcoverallyes.png"), 0.90, -23, 0), 10);
                if (acatcoverallyes) {
                    break;
                }
                else {
                    okclickpop = this.wait(this.getPattern(path.join(__dirname, "./acatc/aatcok.png"), 0.90, 0, 0), 10);
                    if (okclickpop) break;
                }
                if (trytimes - 1 === i) throw "IN ACATC Image :-./acatc/acatcoverallyes.png , ./acatc/acatcacctype.png not found ,Develpoer:- acatcoverallyes -" + acatcoverallyes + " acatctype-" + acatctype + " tries time:-" + i;

            }

            for (let i = 0; i < trytimes; i++) {
                if (!acatcoverallyes) acatcoverallyes = this.wait(this.getPattern(path.join(__dirname, "./acatc/acatcoverallyes.png"), 0.90, -23, 0), 10);
                this.click(acatcoverallyes)

                if (acatcoverallyes) {
                    break;
                } else {
                    okclickpop = this.wait(this.getPattern(path.join(__dirname, "./acatc/aatcok.png"), 0.90, 0, 0), 10);
                    this.click(okclickpop)
                    if (okclickpop) break;
                }
                if (trytimes - 1 === i) throw "IN ACATC Image :-./acatc/aatcok.png , ./acatc/acatcoverallyes.png not found ,Develpoer:- okclickpop -" + okclickpop + " acatcoverallyes-" + acatcoverallyes + " tries time:-" + i;
            }
            let acatcokcancel;
            for (let i = 0; i < trytimes; i++) {
                if (!acatcokcancel) acatcokcancel = this.wait(this.getPattern(path.join(__dirname, "./acatc/acatcokcancel.png"), 0.90, -30, 0), 10);
                this.click(acatcokcancel);
                let acatcmenu = this.wait(this.getPattern(path.join(__dirname, "./acatc/acatcmenu.png"), 0.90, 45, 0), 60);
                if (acatcmenu) {
                    for (let i = 0; i < 6; i++) {
                        this.keys("\b");
                    }
                    return 1;
                }
                if (trytimes - 1 === i) throw "IN ACATC Image :-./acatc/acatcokcancel.png , ./acatc/acatcmenu.png not found ,Develpoer:- acatcokcancel -" + acatcokcancel + " acatcmenu-" + acatcmenu + " tries time:-" + i;

            }

        } catch (error) {
            throw error;
        }
    }
}

// const na = new ACATC();
// na.execute({});
module.exports = ACATC;
// process.exit(0);
