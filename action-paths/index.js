const path = require("path");
ActionPath = require("../../lib/action-path");
class Demo extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    execute(params) {
        try {
           // const params= JSON.stringify(params)
            console.log(params);
            if (params!==undefined) {
                return {
                    status: "ok",
                    data: {
                        params: params,
                    
                    }
                }
            } else {
                return {
                    status: "error",
                    data: {
                        params: params,
                    }
                }
            }

        } catch (error) {
            console.log(error);
            return {
                status: "error",
                error: error
            }
        }

    }
}
module.exports = Demo