const path = require("path");
ActionPath = require("../../lib/action-path");
let sopraLogin = require("../sopralogin/index");
let loginsopra = new sopraLogin();
//const config = require("../../config")
let sopra = "demo1";
let trytimes = 7;
let waitforimage = 60; //sec
let waitinloop = 10;
class TRDP extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    async execute(params, callback = null) {
        try {
            let multibatch = false;
            if (params.batchno) multibatch = true;
            if (!params.batchno) params.batchno = "" + Math.floor(Math.random() * 1000000) + "";
	    // params.batchno = "" + Math.floor(Math.random() * 1000000) + "";

            params.subaccno = "1";
            params.source =  "TIP";
            params.gbltype = params.gbltype ? params.gbltype : "W";
            params.payment = "IT"
            if (params.checknba) {
                params.payment = "T"
            }
            //  let params = {
            //                 batchno: ""+Math.floor(Math.random()*1000000)+"",
            //                 society:"1",
            //                 source:"DUD",
            //                 date:"25-FEB-2019",
            //                 accno:"98000103",
            //                 subaccno: "1",
            //                 gbltype:"r",
            //                 payment:"DD",
            //                 effdate:"26-FEB-2019",
            //                 amt:"100",
            //                 reference:"salary",
            //                 nominated:"sdsdsdsd",
            //                 // nextbatch:"true"

            //              }
            params.nextbatch = true
            if (sopra !== "demo") {
                let login = loginsopra.ensureSopraScreen();
                if (login === 1) {
                    let menu = this.wait(this.getPattern(path.join(__dirname, "./trans/menuprompt.png"), 0.90, 0, 0), waitforimage);
                    this.type('TRDP', menu);
                    this.keys("\n");
                    this.keys("\n");
                    let tdp; let tdpdark;
                    let batchno;
                    let society;
                    let source;
                    let date;
                    let entertrans;
                    let transdate;
                    let accno;
                    let subaccno;
                    let gbltype;
                    let payment;
                    let effdate;
                    let amount;
                    let reference;

                    let favmsg;
                    this.wait(0, 2);

                    for (let i = 0; i < trytimes; i++) {
                        favmsg = this.wait(this.getPattern(path.join(__dirname, "./trans/favmsg.png"), 0.90, 0, 0), 30);
                        if (favmsg) break;
                        if (trytimes - 1 === i) throw "IN TRDP Image :-./trans/favmsg.png  not found , Developer:- favmsg -" + favmsg + " tries time:-" + i;
                    }
                    for (let i = 0; i < trytimes; i++) {
                        tdp = this.wait(this.getPattern(path.join(__dirname, "./trans/tdp.png"), 0.90, 0, 0), 30);
                        tdpdark = this.wait(this.getPattern(path.join(__dirname, "./trans/tdpdark.png"), 0.90, 0, 0), 30);
                        if (tdpdark) this.highlight(tdpdark, null, 3);
                        if (tdp) this.highlight(tdp, null, 3);
                        if (tdp || tdpdark) {

                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/tdp.png, ./trans/tdpdark.png  not found , Developer:- tdp -" + tdp + " tdpdark-" + tdpdark + " tries time:-" + i;
                    }
                    for (let i = 0; i < trytimes; i++) {
                        batchno = this.wait(this.getPattern(path.join(__dirname, "./trans/batchno.png"), 0.90, 10, 0), 30);
                        if (batchno) {
                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/batchno.png  not found , Developer:- batchno -" + batchno + " tries time:-" + i;
                    }
                    let clearbtchinput;
                    if (multibatch) {
                        // let batchvalue = params.batchno;
                        let alertpopup;
                        let outerloopbreak = false;
                        let alertpopup1;
                        for (let i = 0; i < trytimes; i++) {
                            batchno = this.wait(this.getPattern(path.join(__dirname, "./trans/batchno.png"), 0.90, 10, 0), 3);

                            for (let x = 0; x < params.batchno.length; x++) {
                                if (params.batchno[x].is_used !== 2) {
                                    if (i === 0) {

                                        this.type("" + params.batchno[x].batchno + "");
                                    } else {
                                        this.click(batchno);
                                        this.type("" + params.batchno[x].batchno + "", batchno);
                                    }

                                    this.keys("\t");
                                    this.keys("\t");

                                    alertpopup = this.wait(this.getPattern(path.join(__dirname, "./trans/alertpopbatch.png"), 0.85, 0, 0), 5);
                                    if (alertpopup) {
                                        params.batchno[x].is_used = 2;
                                        for (let j = 0; j < trytimes; j++) {
                                            this.click(alertpopup);
                                            alertpopup1 = this.wait(this.getPattern(path.join(__dirname, "./trans/alertpopbatch.png"), 0.85, 0, 0));
                                            if (!alertpopup1) break;
                                            if (trytimes - 1 === j) throw "failed to click alert pop";
                                        }
                                        for (let k = 0; k < trytimes; k++) {
                                            clearbtchinput = this.wait(this.getPattern(path.join(__dirname, "./trans/clearbtchinput.png"), 0.88, 39, 0),3);
                                            this.doubleClick(clearbtchinput);
                                            this.keys("\b");
                                            batchno = this.wait(this.getPattern(path.join(__dirname, "./trans/batchno.png"), 0.90, 10, 0));
                                            if (batchno) break;
                                            if (trytimes - 1 === k) throw " failed to clear batch input";
                                        }

                                    } else if (!alertpopup) {
                                        params.batchno[x].is_used = 2;
                                        outerloopbreak = true;
                                        break;
                                    }
                                }
                                if (params.batchno.length - 1 === x) throw " all batch  number is in used";

                            }
                            if (outerloopbreak) break;
                            if (trytimes - 1 === i) throw " invalid batch number provided";
                        }


                    } else {
                        this.type(params.batchno);
                    }

                    // for(let i=0;i<trytimes;i++){
                    //     society= this.wait(this.getPattern(path.join(__dirname, "./trans/society.png"),0.90,16,0));
                    //     this.highlight(society);
                    //     if(society){
                    //         break;
                    //     }
                    //     if(trytimes-1 === i) throw new Error("acvmenu  not found line 57") 
                    // }
                    // this.type(params.society,society);
                    let checkdate = true;
                    let trdsourceot;
                    for (let i = 0; i < trytimes; i++) {
                        if(!source){
                            source = this.wait(this.getPattern(path.join(__dirname, "./trans/source.png"), 0.85, 0, 0),3);
                            this.highlight(source, null, 2);
                        }
                        this.click(source);
                        if(!trdsourceot){
                           trdsourceot = this.wait(this.getPattern(path.join(__dirname, "./trans/trdpsource.png"), 0.85, 0, 0),5);
                        }
                        if (source || trdsourceot) {
                            let sourcetype = (source) ? this.type(params.source, source) : this.type(params.source, trdsourceot);
                            if ((source || trdsourceot) && sourcetype) {
                                break;
                            }
                        } else if ((!source || !trdsourceot) && i>2) {
                            checkdate = false;
                            break;
                        }

                        if (trytimes - 1 === i) throw " batch number in used"
                    }
                    if (checkdate) {
                        for (let i = 0; i < trytimes; i++) {
                            date = this.wait(this.getPattern(path.join(__dirname, "./trans/date.png"), 0.90, 0, 0));
                            if (date) {
                                this.type(params.date, date);
                                break;
                            }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/date.png  not found , Developer:- date -" + date + " tries time:-" + i;
                        }
                       
                    }


                    //  let checkdetail= this.wait(this.getPattern(path.join(__dirname, "./trans/checkdetail.png"),0.90,62,-2));
                    // this.type('OB',checkdetail);

                    //-------------------------------------------------------------------------// 
                    let entertran2;
                    for (let i = 0; i < trytimes; i++) {
                        if (!entertran2) {
                            entertrans = this.wait(this.getPattern(path.join(__dirname, "./trans/entertrans.png"), 0.90, 0, 0));
                            this.click(entertrans);
                        }
                        if (!entertrans) {
                            entertran2 = this.wait(this.getPattern(path.join(__dirname, "./trans/entertran2.png"), 0.90, 0, 0));
                            this.click(entertran2);
                        }
                        transdate = this.wait(this.getPattern(path.join(__dirname, "./trans/transdate.png"), 0.90, 0, 0));
                        if (transdate) {
                            break;
                        }
                        if (trytimes - 1 === i) throw "transcation failed due to all provide batchno is in used";
                    }
                    let datetrans = params.transaction_date ? params.transaction_date : params.date

                    this.type(datetrans, transdate);
                    this.keys("\t");
                    let okalertpopup = this.wait(this.getPattern(path.join(__dirname, "./trans/daterr.png"), 0.90, 0, 0), 3);
                    let tranerror = this.wait(this.getPattern(path.join(__dirname, "./trans/tranerror.png"), 0.90, 0, 0), 3);

                    if (okalertpopup && tranerror) {
                        for (let i = 0; i < trytimes; i++) {
                            okalertpopup = this.wait(this.getPattern(path.join(__dirname, "./trans/daterr.png"), 0.90, 0, 0), 5);
                            let clickalert = this.click(okalertpopup)
                            let okalertpopup1 = this.wait(this.getPattern(path.join(__dirname, "./trans/daterr.png"), 0.90, 0, 0));
                            if (!okalertpopup1 && clickalert) break;
                        }
                        let toveriride;
                        for (let i = 0; i < trytimes; i++) {
                            let toveriridenew = this.wait(this.getPattern(path.join(__dirname, "./trans/toveriride.png"), 0.90, 0, 0));
                            if (toveriridenew) {
                                toveriride = toveriridenew
                            } else {
                                toveriride = this.wait(this.getPattern(path.join(__dirname, "./trans/highightoverride.png"), 0.90, 0, 0));
                            }
                            let overideclick = this.click(toveriride);
                            if (overideclick && toveriride) break;
                        }
                    }

                    for (let i = 0; i < trytimes; i++) {
                        let accno1 = this.wait(this.getPattern(path.join(__dirname, "./trans/accno.png"), 0.90, 0, 0));
                        if (accno1) { accno = accno1; }
                        else {
                            accno = this.wait(this.getPattern(path.join(__dirname, "./trans/trnsaccno2.png"), 0.90, 0, 0));
                        }
                        // this.highlight(accno);
                        if (accno) { break; }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/accno.png, ./trans/trnsaccno2.png   not found , Developer:- accno -" + accno + " accno1-" + accno1 + " tries time:-" + i;
                    }
                    this.type(params.account_number, accno);
                    for (let i = 0; i < trytimes; i++) {
                        subaccno = this.wait(this.getPattern(path.join(__dirname, "./trans/subaccno.png"), 0.90, 20, 0));
                        if (subaccno) {
                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/subaccno.png   not found , Developer:- subaccno -" + subaccno + " tries time:-" + i;
                    }
                    this.type(params.subaccno, subaccno);
                    for (let i = 0; i < trytimes; i++) {
                        gbltype = this.wait(this.getPattern(path.join(__dirname, "./trans/gbltype.png"), 0.90, 30, 0));
                        if (gbltype) { break; }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/gbltype.png   not found , Developer:- gbltype -" + gbltype + " tries time:-" + i;
                    }
                    this.type(params.gbltype, gbltype);
                    for (let i = 0; i < trytimes; i++) {
                        payment = this.wait(this.getPattern(path.join(__dirname, "./trans/payment.png"), 0.90, 30, 0));
                        if (payment) { break; }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/payment.png   not found , Developer:- payment -" + payment + " tries time:-" + i;
                    }
                    this.type(params.payment, payment);
                    this.keys("\t");
                    let transaccdtl;
                    if(!params.checknba) transaccdtl= this.wait(this.getPattern(path.join(__dirname, "./trans/transaccdtl.png"), 0.90, 0, 0), 20);
                    if (transaccdtl && !params.checknba) {
                        this.type(params.transferaccount);
                        this.keys("\t");
                        this.type(params.subaccno);
                        this.keys("\t");
                        this.keys("\t");
                        //    for(let i=0;i<trytimes;i++){
                        //     let  tranotoverride  =  this.wait(this.getPattern(path.join(__dirname, "./trans/tranotoverride.png"),0.90,0,0),10);
                        //     let clicktranotoverride =this.click(tranotoverride);
                        //     let  tranotoverride1  =  this.wait(this.getPattern(path.join(__dirname, "./trans/tranotoverride.png"),0.90,0,0),10);
                        //     if(tranotoverride1 && clicktranotoverride) break;
                        //     if(trytimes-1 === i) throw "  not found line 160";
                        //    }
                    }



                    for (let i = 0; i < trytimes; i++) {
                        if (this.wait(this.getPattern(path.join(__dirname, "./trans/transamout1.png"), 0.90, 0, 0))) {
                            amount = this.wait(this.getPattern(path.join(__dirname, "./trans/transamout1.png"), 0.90, 0, 0),2)
                        } else {
                            amount = this.wait(this.getPattern(path.join(__dirname, "./trans/amount.png"), 0.90, 0, 0),2);
                        }
                        if (amount) {

                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/transamout1.png, ./trans/amount.png  not found , Developer:- amount -" + amount + " tries time:-" + i;
                    }
                    this.type(params.amount, amount);
                    if (!params.checknba) {
                        for (let i = 0; i < trytimes; i++) {
                            reference = this.wait(this.getPattern(path.join(__dirname, "./trans/reference.png"), 0.90, 0, 0),2);
                            if (amount) {

                                break;
                            }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/reference.png  not found , Developer:- reference -" + reference + " tries time:-" + i;
                        }
                        this.type(params.reference, reference);
                    }

                    for (let i = 0; i < trytimes; i++) {
                        let posttrans = this.wait(this.getPattern(path.join(__dirname, "./trans/posttrans.png"), 0.90, 0, 0),3);
                        if (posttrans) {
                            let posttrnsclick = this.click(posttrans);
                            if (posttrnsclick) break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/posttrans.png  not found , Developer:- posttrans -" + posttrans + " tries time:-" + i;
                    }
                    let warningtrans = this.wait(this.getPattern(path.join(__dirname, "./trans/warningtrans.png"), 0.90, 0, 0), 5);
                    if (warningtrans) {
                        for (let i = 0; i < trytimes; i++) {
                            let postdatealert = this.wait(this.getPattern(path.join(__dirname, "./trans/postdatealert.png"), 0.90, 0, 0),5);
                            let postclickalrt = this.click(postdatealert);
                            let postdatealert1 = this.wait(this.getPattern(path.join(__dirname, "./trans/postdatealert.png"), 0.90, 0, 0),2);
                            if (!postdatealert1 && postclickalrt) break;
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/postdatealert.png  not found , Developer:- postdatealert -" + postdatealert + " tries time:-" + i;
                        }
                    }

                    let limitvalue = this.wait(this.getPattern(path.join(__dirname, "./trans/limitvalue.png"), 0.85, 25, 0), 5);
                    let limitvalue1;
                    let postdatealertn;
                    let maxvalue = this.wait(this.getPattern(path.join(__dirname, "./trans/postdatealert.png"), 0.85, 0, 0), 5);
                    if (limitvalue) {
                        let datetime = new Date().getTime();
                        params.img = "balance" + datetime;
                        this.capture(params.img);
                        for (let i = 0; i < trytimes; i++) {
                            if (!postdatealertn) {
                                limitvalue1 = this.wait(this.getPattern(path.join(__dirname, "./trans/limitvalue.png"), 0.85, 25, 0), 6);
                                this.click(limitvalue1);
                            }
                            postdatealertn = this.wait(this.getPattern(path.join(__dirname, "./trans/postdatealert.png"), 0.85, 0, 0),3);
                            if (postdatealertn) {
                                let postclick = this.click(postdatealertn);
                                if (postclick) break;
                            }
                            if (trytimes - 1 === i) throw "account balance is less  than required balance";
                        }
                        throw "account balance is less than required balance";
                    }
                    if (maxvalue) {
                            let datetime = new Date().getTime();
                            params.img = "invalid" + datetime;
                            this.capture(params.img);
                        for (let i = 0; i < trytimes; i++) {
                            if (!postdatealertn) postdatealertn = this.wait(this.getPattern(path.join(__dirname, "./trans/postdatealert.png"), 0.85, 0, 0),3);
                            limitvalue1 = this.wait(this.getPattern(path.join(__dirname, "./trans/limitvalue.png"), 0.85, 25, 0), 3);
                            if (postdatealertn && !limitvalue1) {
                                let postclick = this.click(postdatealertn);
                                if (postclick) break;
                            } else if(limitvalue1){
                                this.click(limitvalue1);
                            }
                            if (trytimes - 1 === i) throw "invalid trans type";
                        }
                        throw "invalid trans type";
                    }

                    //-------------------------------------------------------------------------//
                    if (params.nextbatch) {
                        let close;
                        let menutrdp;
                        for (let i = 0; i < trytimes; i++) {
                            let batchdone = this.wait(this.getPattern(path.join(__dirname, "./trans/batchdone.png"), 0.90, 0, 0),3);
                            let clickbatch = this.click(batchdone);
                            close = this.wait(this.getPattern(path.join(__dirname, "./trans/close.png"), 0.90, 0, 0),3);
                            if (clickbatch && close) { break; }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/batchdone.png, ./trans/close.png  not found , Developer:- batchdone -" + batchdone + " close-" + close + " tries time:-" + i;
                        }
                        for (let i = 0; i < trytimes; i++) {
                            close = this.wait(this.getPattern(path.join(__dirname, "./trans/close.png"), 0.90, 0, 0));
                            let closeclick = this.click(close);
                            menutrdp = this.wait(this.getPattern(path.join(__dirname, "./trans/menutrdp.png"), 0.90, 0, 0), waitforimage);
                            if (closeclick && menutrdp) { break; }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/close.png, ./trans/menutrdp.png  not found , Developer:- close -" + close + " menutrdp-" + menutrdp + " tries time:-" + i;

                        }
                        for (let i = 0; i < trytimes; i++) {
                            menutrdp = this.wait(this.getPattern(path.join(__dirname, "./trans/menutrdp.png"), 0.90, 0, 0), waitforimage);
                            if (menutrdp) {
                                for (let i = 0; i < 5; i++) {
                                    this.keys("\b");
                                }
                                break;
                            }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/menutrdp.png  not found , Developer:- menutrdp -" + menutrdp + " tries time:-" + i;
                        }
                    }


                    return {
                        status: "ok", message: "successfully", data: params
                    }
                } else if (login === -1) {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "login" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    this.hardClose("iexplore.exe");
                    // loginsopra.ensureSopraScreen();
                    params.sopraerror = "login failed"
                    return {
                        status: "-1", message: "rpa_agent_failed", data: params
                    }
                } else if (login === -2) {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "Soprainstancefailed" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    this.hardClose("iexplore.exe");
                    // loginsopra.ensureSopraScreen();
                    params.sopraerror = "Sopra instance is not available"
                    return {
                        status: "-1", message: "rpa_agent_failed", data: params
                    }
                }
                else {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "networkerr" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    this.hardClose("iexplore.exe");
                    // loginsopra.ensureSopraScreen();
                    params.sopraerror = "newtork error occured";
                    return {
                        status: "error",
                        data: params,
                        message: "rpa_agent_failed"
                    }

                }
            } else {
                setTimeout(function () { return { status: "ok", data: params } }, 10000);
            }

        } catch (error) {
            try {
                if(!params.img){
                    let datetime = new Date().getTime();
                    params.img = error.replace(/\s/g, '').substr(0, 6) + datetime;
                    this.capture(params.img);
                }
            } catch (e) { }
            // Taskkill /F /IM iexplore.exe
            this.hardClose("iexplore.exe");
            loginsopra.ensureSopraScreen();
            // console.log(error);
            this.debugerror(error);
            params.sopraerror = JSON.stringify(error);
            params.screen = this.getScreen();
            return {
                status: "error",
                data: params,
                message: "rpa_agent_failed"
            }
        }

    }
}

// const na = new TRDP();
// na.execute({});
module.exports = TRDP;
//process.exit(0);