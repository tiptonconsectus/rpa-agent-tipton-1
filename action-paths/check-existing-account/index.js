const path = require("path");
ActionPath = require("../../lib/action-path");
let sopraLogin = require("../sopralogin/index");
let loginsopra = new sopraLogin();
let trytimes = 7;
let sopra = "demo1";

class CheckExisting extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    execute(params, callback = null) {
        try {
            if (sopra !== "demo") {
                let login = loginsopra.ensureSopraScreen();
                if (login === 1) {

                    params.accountno = (typeof params.accountno === "string") ? params.accountno : "" + params.accountno + "";
                    let menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.90, 0, 0), 60);
                    this.type('CUM', menu);
                    this.keys("\n");
                    this.keys("\n");
                    let cumacc = this.wait(this.getPattern(path.join(__dirname, "./cum/cumacc.png"), 0.90, 0, 0), 60);
                    this.type(params.accountno, cumacc);
                    //   this.type(param.firstname.trim().charAt(0).toUpperCase(), initials);
                    let cumtitle;
                    let cumalpha;
                    let cumsurname;
                    let cumdob;
                    for (let i = 0; i < trytimes; i++) {
                        cumalpha = this.wait(this.getPattern(path.join(__dirname, "./cum/cumcustomer.png"), 0.90, 0, 0), 5);
                        let cuminitialtype = this.type(params.lastname + " " + params.firstname.trim().charAt(0).toUpperCase() + "%", cumalpha);
                        if (cumalpha && cuminitialtype) {
                            break;
                        }
                        if (trytimes - 1 === i) throw "cum inital name not found";
                    }
                   
                    for (let i = 0; i < trytimes; i++) {
                        cumdob = this.wait(this.getPattern(path.join(__dirname, "./cum/cumdob.png"), 0.90, 0, 0), 10);

                        let cumdobtype = this.type(params.dob, cumdob);
                        if (cumdob && cumdobtype) break;
                        if (trytimes - 1 === i) throw "cum dob not found";
                    }
                    //  let cumcustacc = this.wait(this.getPattern(path.join(__dirname, "./cum/cumcustacc.png"), 0.90, 0, 0), 60);
                    //   this.type(params.accountno,cumacc);
                    let cumsearch;
                    let cumgo;
                    let cumidentity;
                    let cumother;
                    let cummobile;
                    for (let i = 0; i < trytimes; i++) {
                        cumsearch = this.wait(this.getPattern(path.join(__dirname, "./cum/cumsearch.png"), 0.90, 0, 0), 5);
                        let clicksearch = this.click(cumsearch);
                        cumgo = this.wait(this.getPattern(path.join(__dirname, "./cum/cumgo.png"), 0.90, 0, 0), 5);
                        if (clicksearch && cumgo) break;
                        if (trytimes - 1 === i) throw "CUM account not exists in sopra"
                    }
                    let cumidentitycheck;
                    let cumcheckclosebtn = false;
                    for (let i = 0; i < trytimes; i++) {
                        cumgo = this.wait(this.getPattern(path.join(__dirname, "./cum/cumgo.png"), 0.90, 0, 0), 10);
                        let cumgoclick = this.click(cumgo);
                        cumidentity = this.wait(this.getPattern(path.join(__dirname, "./cum/cumidentity.png"), 0.90, 0, 0), 3);
                        cumidentitycheck = this.wait(this.getPattern(path.join(__dirname, "./cum/cumidentitycheck.png"), 0.90, 0, 0), 2);
                        if (cumidentitycheck) {
                            cumcheckclosebtn = true;
                        }
                        if (cumidentity || cumidentitycheck) break;
                        if (trytimes - 1 === i) throw " CUM not found line 42"
                    }
                    if (cumcheckclosebtn) {
                        for (let i = 0; i < trytimes; i++) {
                            let cumalertinfo = this.wait(this.getPattern(path.join(__dirname, "./cum/cumalertinfo.png"), 0.90, 0, 0), 3);
                            let cumclosebtn1 = this.wait(this.getPattern(path.join(__dirname, "./cum/cumclosebtn1.png"), 0.90, 0, 0), 3);
                            this.click(cumclosebtn1);
                            cumalertinfo = this.wait(this.getPattern(path.join(__dirname, "./cum/cumalertinfo.png"), 0.90, 0, 0), 3);
                            if (!cumalertinfo) break;
                        }
                    }
                    // let customeraltinfo =  this.wait(this.getPattern(path.join(__dirname, "./cum/cumgo.png"), 0.90, 0, 0), 10);
                    let found;


                    if (params.contactno) {
                        let mobileno;
                        let emptymobileno;
                        for (let i = 0; i < trytimes; i++) {
                            mobileno = this.wait(this.getPattern(path.join(__dirname, "./cum/mobilenoupdate.png"), 0.90, 57, 0), 5);
                            this.click(mobileno);
                            this.doubleClick(mobileno);
                            this.keys("\b");
                            emptymobileno = this.wait(this.getPattern(path.join(__dirname, "./cum/emptymobileno.png"), 0.90, 0, 0), 2);
                            if (emptymobileno) break;
                        }
                        this.type(params.contactno, emptymobileno);
                    }
                    if (!params.updatenumber) {
                        let existcustomer = ""
                        for (let i = 0; i < 3; i++) {
                            let customerIDpath = this.wait(this.getPattern(path.join(__dirname, "./cum/existcustomer.png"), 0.85, 64, 0), 5);
                            existcustomer = this.FindImageText(customerIDpath);
                            if (parseInt(existcustomer)) break;
                            // if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/customerID.png, not found , Developer:-  customerIDpath-" + customerIDpath + " tries time:-" + i;
                        }
                        params.customersid = existcustomer.trim();

                        for (let i = 0; i < trytimes; i++) {
                            let cumcheckmobileyes;
                            cumother = this.wait(this.getPattern(path.join(__dirname, "./cum/cumother.png"), 0.90, 0, 0), 3);
                            let cumotherclick = this.click(cumother);
                            cummobile = this.wait(this.getPattern(path.join(__dirname, "./cum/cummobile.png"), 0.90, 55, 0), 3);

                            if (!cummobile) cumcheckmobileyes = this.wait(this.getPattern(path.join(__dirname, "./cum/cumcheckmobileyes.png"), 0.90, 0, 0), 5);
                            if ((cumotherclick && cummobile) || cumcheckmobileyes) {
                                if (cumcheckmobileyes) found = true;
                                break;
                            }
                            if (trytimes - 1 === i) throw " not found line 49"
                        }
                        if (!found) this.type("YES", cummobile);
                        this.keys("\t");
                    }
                    let cumokapply;
                    let cummenu;
                    for (let i = 0; i < trytimes; i++) {
                        cumokapply = this.wait(this.getPattern(path.join(__dirname, "./cum/cumokapply.png"), 0.90, -83, 0), 10);
                        let cumokapplyclick = this.click(cumokapply);
                        cummenu = this.wait(this.getPattern(path.join(__dirname, "./cum/cummenu.png"), 0.90, 30, 0));
                        if (cummenu && cumokapplyclick) break;
                        if (trytimes - 1 === i) throw " not found line 61"
                    }
                    for (let i = 0; i < trytimes; i++) {
                        cummenu = this.wait(this.getPattern(path.join(__dirname, "./cum/cummenu.png"), 0.90, 30, 0), 10);
                        if (cummenu) {
                            for (let i = 0; i < 5; i++) {
                                this.keys("\b");
                            }
                            break;
                        }
                        if (trytimes - 1 === i) throw " not found line 71 in CUM menu"
                    }

                    return {
                        status: "ok", message: "successfully", data: params, isexists: true
                    }

                } else if (login === -1) {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "login" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    params.sopraerror = "login failed";
                    this.hardClose("iexplore.exe");
                    return {
                        status: "-1", message: "rpa_agent_failed", data: params
                    }
                } else if (login === -2) {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "Soprainstancefailed" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    this.hardClose("iexplore.exe");
                    params.sopraerror = "Sopra instance is not available"
                    return {
                        status: "-1", message: "rpa_agent_failed", data: params
                    }
                }
                else {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "networkerr" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    this.hardClose("iexplore.exe");
                    // loginsopra.ensureSopraScreen();
                    params.sopraerror = "newtork error occured";
                    return {
                        status: "error",
                        data: params,
                        message: "rpa_agent_failed"
                    }
                }
            } else {
                return {
                    status: "ok", message: "successfully", data: params, isexists: true
                }
            }


        } catch (error) {
            try {
                let datetime = new Date().getTime();
                params.img = error.replace(/\s/g, '').substr(0, 6) + datetime;
                this.capture(params.img);
            } catch (e) { }
            // console.log(error)
            this.hardClose("iexplore.exe");
            loginsopra.ensureSopraScreen();

            params.sopraerror = JSON.stringify(error);
            params.screen = this.getScreen();
            return {
                status: "error",
                data: params,
                message: "rpa_agent_failed",
                isexists: false
            }
        }

    }
}

module.exports = CheckExisting