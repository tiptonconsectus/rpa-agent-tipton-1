const path = require("path");
ActionPath = require("../../lib/action-path");
let newacc = require("../newaccount/index.js");
let newAcc = new newacc();
let sopraLogin = require("../sopralogin/index");
let loginsopra = new sopraLogin();
const config = require("../../config/config");
let Bcam = require("../BCAM/index.js");
let bcam = new Bcam();
// config.sopra ="demo";
class accCallCredit extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    execute(params, callback = null) {
        try {
            params.accno = (typeof params.account_number === "string") ? params.account_number : "" + params.account_number + "";
            params.accountcode = (params.accountcode) ? params.accountcode : "SF01";

            if (params.ratechangeperiod && params.ratechangeperiod.period) params.ratechangeperiod.period = params.ratechangeperiod.period;
            if (params.ratechangeperiod && params.ratechangeperiod.value) params.ratechangeperiod.value = params.ratechangeperiod.value;
            else params.ratechangeperiod = { period: "D", value: "30" }

            if (params.noticeperiod && params.noticeperiod.period) params.noticeperiod.period = params.noticeperiod.period;
            if (params.noticeperiod && params.noticeperiod.value) params.noticeperiod.value = params.noticeperiod.value;
            else params.noticeperiod = { period: "D", value: "30" }

            if (config.sopra === "demo1") {

                if (params.callcredit !== undefined) {
                    params.status = (params.status !== undefined) ? 1 : 0
                    setTimeout(function () { return { status: "ok", data: "reject less score found" } }, 2000)
                } else {
                    params.status = (params.status !== undefined) ? 1 : 0
                    setTimeout(function () { return { status: "ok", data: "reject less score found" } }, 2000)
                }
            } else {
                let login = loginsopra.ensureSopraScreen();
                if (login === 1) {

                    params = newAcc.execute(params, 0, callback);
                    bcam.execute(params);
                    return {
                        status: "ok", message: "successfully", data: params
                    }
                } else if (login === -1) {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "login" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    this.hardClose("iexplore.exe");
                    params.sopraerror = "login failed"
                    return {
                        status: "-1", message: "rpa_agent_failed", data: params
                    }
                } else if (login === -2) {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "Soprainstancefailed" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    this.hardClose("iexplore.exe");
                    params.sopraerror = "Sopra instance is not available"
                    return {
                        status: "-1", message: "rpa_agent_failed", data: params
                    }
                } else {
                    try {
                        let datetime = new Date().getTime();
                        params.img = "networkerr" + datetime;
                        this.capture(params.img);
                    } catch (e) { }
                    this.hardClose("iexplore.exe");
                    // loginsopra.ensureSopraScreen();
                    params.sopraerror = "newtork error occured";
                    return {
                        status: "error",
                        data: params,
                        message: "rpa_agent_failed"
                    }
                }
            }

        } catch (error) {
            try {
                let datetime = new Date().getTime();
                params.img = error.replace(/\s/g, '').substr(0, 6) + datetime;
                this.capture(params.img);
            } catch (e) { }
            this.hardClose("iexplore.exe");
            loginsopra.ensureSopraScreen();
            // console.log(error);
            params.sopraerror = JSON.stringify(error);
            params.screen = this.getScreen();
            return {
                status: "error",
                data: params,
                message: "rpa_agent_failed"
            }
        }

    }
}
module.exports = accCallCredit