const path = require("path");
ActionPath = require("../../lib/action-path");
let Bcam = require("../BCAM/index.js");
let bcam = new Bcam();
//const config = require("../../config/config.js")
let sopra = "demo1";
let trytimes = 7;
let waitforimage = 60; //sec
let waitinloop = 10;
class NewAccount extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    execute(params, isexisting = 0, callback = null) {
        // let first = Math.random().toString(36).slice(2);
        // let last = Math.random().toString(36).slice(2);
        // let natno = Math.floor(100000 + Math.random() * 9000);
        try {

            if (sopra !== "demo") {
                // let params = {
                //     "accno": "98000103",
                //     "accountcode": "SF01",
                //     "dateofbirth": "26-nov-1991",
                //     "title": "Mr",
                //     "gender": "male",
                //     "firstname": first,
                //     "middlename": "",
                //     "lastname": last,
                //     //country : gb (short code)
                //     "address": [
                //         {
                //             "houseno": "2",
                //             "street": "v.N PURAV1B",
                //             "city": "Thane1b",
                //             "postalcode": "400068",
                //             "islivetwelemonth": 1
                //         }
                //     ],

                //     "nationalinsuranceno": "AB"+natno+"A",
                //     "emailid": "bipin.shukla@consectus.com",
                //     "mobileno": "9987379164",
                //     // "mobileno2": "9987379164",
                //     "istermsandcondition": true,
                //     "nominatedbankno": "hhggbb",
                //     "accountsortcode": "CON32",
                //     "accountno": "1234567890",
                //     "accountholdername": "BIPIN SHUKLA",
                //     "new_accounts_id": 1075
                // }

                // let login =  loginsopra.ensureSopraScreen();
                // if(login === 1){

                // await callback.sendStep(params.callbackpath, "1/6");

                this.ACHC(params);
                // await callback.sendStep(params.callbackpath, "2/6");
                this.SASC(params);
                //   await callback.sendStep(params.callbackpath, "3/6");
                this.ACLM(params, isexisting);
                //  await callback.sendStep(params.callbackpath, "4/6");
                this.ACV(params);
               
                if (isexisting) {
                    if (params.customersid && params.existing_update) {
                        if (params.existing_update.nationalinsuranceno && params.existing_update.emailid) {
                            this.updateExsting(params.existing_update, params.customersid)
                        }
                        if (params.existing_update.nominatedbankno && params.existing_update.accountsortcode && params.existing_update.accountholdername) {
                            params.existing_update.accno = params.accno;
                            bcam.execute(params.existing_update);
                        }
                    }
                }

                return params

                // return {
                //     status:"ok",  message:"successfully",data :params
                // }
                // }
                // else{
                //     params.sopraerror = "newtork error occured";
                //     return {
                //         status: "error",
                //         data: params,
                //         message: "rpa_agent_failed"
                //     }

                // }

            } else {
                params.customersid = Math.floor(Math.random() * Math.floor(99999999));
                params.account_number = Math.floor(Math.random() * Math.floor(99999999));

                console.log(params)
                let promise = new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        resolve({ status: "ok", data: params });
                    }, 1000);
                });

                return promise;
            }

        } catch (error) {

            throw error
        }


    }
    ACHC(param) {

        let menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.90, 0, 0), 200);
        this.type('ACHC', menu);
        this.keys("\n");
        this.keys("\n");
        let overall
        for (let i = 0; i < trytimes; i++) {
            let society = this.wait(this.getPattern(path.join(__dirname, "./newaccount/society.png"), 0.90, 0, 0), 10);

            if (society) {
                this.type('1', society);
                break;
            }
            if (trytimes - 1 === i) throw "In ACHC Image:- ./newaccount/society.png not found , Developer:- society -" + society + " tries time:-" + i
        }
        for (let i = 0; i < trytimes; i++) {
            overall = this.wait(this.getPattern(path.join(__dirname, "./newaccount/overall.png"), 0.90, 27, 0), 10);

            if (overall) {
                this.type(param.accountcode, overall);
                break;
            }
            if (trytimes - 1 === i) throw "In ACHC Image:- ./newaccount/overall.png not found , Developer:- overall-" + overall + " tries time:-" + i;
        }

        let accno = this.wait(this.getPattern(path.join(__dirname, "./newaccount/accno.png"), 0.90, 0, 0), 60);

        this.click(accno)
        this.type(param.accno, accno);
        let postcode;
        for (let i = 0; i < trytimes; i++) {
            let addbutton = this.wait(this.getPattern(path.join(__dirname, "./newaccount/addbutton.png"), 0.90, 0, 0), 10);
            this.click(addbutton);
            postcode = this.wait(this.getPattern(path.join(__dirname, "./newaccount/postcode.png"), 0.90, 0, 0), 10);
            if (postcode) {
                break;
            }
            if (trytimes - 1 === i) throw "it seems like accountno or overall A/c type:-" + param.accountcode + " is not correct , Developer:- addbutton -" + addbutton + " postcode -" + postcode + " tries time:-" + i;
        }
        this.type(param.address[0].postalcode, postcode);
        let houseno = this.wait(this.getPattern(path.join(__dirname, "./newaccount/houseno.png"), 0.90, 20, 0), 60);
        this.type(param.address[0].houseno, houseno);

        let none;
        let add1;
        for (let i = 0; i < trytimes; i++) {

            if (!none && !add1) {
                let search = this.wait(this.getPattern(path.join(__dirname, "./newaccount/search.png"), 0.90, 0, 0), 10);
                this.click(search);
            }

            none = this.wait(this.getPattern(path.join(__dirname, "./newaccount/none.png"), 0.90, 0, 0), 10);
            if (none) {
                break;
            } else {
                add1 = this.wait(this.getPattern(path.join(__dirname, "./newaccount/add1.png"), 0.90, 0, 8), 10);
                if (add1) {
                    break;
                }

            }
            if (trytimes - 1 === i) throw "In ACHC Image:- /newaccount/search.png, ./newaccount/none.png, ./newaccount/add1.png not found , Developer:- search-" + search + " none-" + none + " add1-" + add1 + " tries time:-" + i;
        }
        let branch;
        if (none !== false) {
            none = this.wait(this.getPattern(path.join(__dirname, "./newaccount/none.png"), 0.90, 0, 0), 5);
            this.click(none);
            this.keys("u\"\ue002\"");
            for (let i = 0; i < trytimes; i++) {
                let selectexistingadd = this.wait(this.getPattern(path.join(__dirname, "./newaccount/selectexistingadd.png"), 0.90, 0, 0), 10);
                this.click(selectexistingadd);
                branch = this.wait(this.getPattern(path.join(__dirname, "./newaccount/branch.png"), 0.90, 0, 0), 10);
                if (branch) {
                    break;
                }
                if (trytimes - 1 === i) throw "In ACHC Image:-  ./newaccount/branch.png, ./newaccount/selectexistingadd.png not found , Developer:- selectexistingadd-" + selectexistingadd + " branch-" + branch + " tries time:-" + i;
            }
        } else {
            this.type(param.address[0].houseno, add1);
            this.keys("\t");
            // let add2 =this.wait(path.join(__dirname, "./newaccount/add2.png"));
            this.type(param.address[0].street);
            this.keys("\t");
            //  let add3 =this.wait(path.join(__dirname, "./newaccount/add3.png"));
            this.type(param.address[0].city);
            //  let add4 =this.wait(path.join(__dirname, "./newaccount/add4.png"));
            //  this.type('SL64AT', add4);
            //  let add5 =this.wait(path.join(__dirname, "./newaccount/add5.png"));
            //  this.type('SL64AT', add5);
            let postcodeinput = this.wait(this.getPattern(path.join(__dirname, "./newaccount/postcodeinput.png"), 0.90, 0, 0), 60);
            this.type(param.address[0].postalcode, postcodeinput);

            let recordappliedok;
            let duplicatefile;
            let createadd;
            for (let i = 0; i < trytimes; i++) {
                if (!recordappliedok && !duplicatefile) {
                     createadd = this.wait(this.getPattern(path.join(__dirname, "./newaccount/createadd.png"), 0.90, 0, 0), 5);
                    this.click(createadd);
                }
                recordappliedok = this.wait(this.getPattern(path.join(__dirname, "./newaccount/recordappliedok.png"), 0.90, 160, 50), 3);
               // console.log("finded recordappliedok")
                if (!recordappliedok) {
                    duplicatefile = this.wait(this.getPattern(path.join(__dirname, "./newaccount/duplicatefile.png"), 0.90, -20, 0), 3);
                    this.click(duplicatefile);
                  //  console.log("duplicated finded recordappliedok")
                }
                if (recordappliedok) {
                    this.click(recordappliedok);
                    branch = this.wait(this.getPattern(path.join(__dirname, "./newaccount/branch.png"), 0.90, 0, 0), 3);
                    if (branch) {
                        break;
                    }
                }
                if (trytimes - 1 === i) throw "In ACHC Image:-  ./newaccount/recordappliedok.png, ./newaccount/createadd.png, ./newaccount/branch.png not found , Developer:- recordappliedok-" + recordappliedok + "createadd-" + createadd + " branch-" + branch + " tries time:-" + i;
            }
        }
        this.type('APP', branch);

        let acctitle = this.wait(this.getPattern(path.join(__dirname, "./newaccount/acctitle.png"), 0.90, 0, 0), 60);
        this.type(param.accountname, acctitle);
        this.keys("\t");
        this.type(param.accountname)
        let ratechangenotice = this.wait(this.getPattern(path.join(__dirname, "./newaccount/ratechangenotice.png"), 0.90, 25, 0), 60);
        this.type(param.ratechangeperiod.period, ratechangenotice);

        this.keys("\t");
        this.type(param.ratechangeperiod.value);
        this.keys("\t");

        let achc_in_menu
        for (let i = 0; i < trytimes; i++) {
            let achc_save = this.wait(this.getPattern(path.join(__dirname, "./newaccount/achcsave.png"), 0.90, 0, 0), 10);
            this.click(achc_save);
            achc_in_menu = this.wait(this.getPattern(path.join(__dirname, "./newaccount/achcinmenu.png"), 0.90, 80, 0), 10);
            if (achc_in_menu) {
                this.click(achc_in_menu);
                for (let x = 0; x <= 4; x++) {
                    this.keys("\b")
                }
                break;
            }
            if (trytimes - 1 === i) throw "In ACHC Image:-  ./newaccount/achcsave.png, ./newaccount/achcinmenu.png not found , Developer:- achc_save-" + achc_save + " achc_in_menu-" + achc_in_menu + " tries time:-" + i;
        }


    }
    SASC(param) {

        let menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.90, 0, 0), 200);
        this.type('SASC', menu);
        this.keys("\n");
        this.keys("\n");
        let accno;
        for (let i = 0; i < trytimes; i++) {
            let society = this.wait(this.getPattern(path.join(__dirname, "./sasc/society.png"), 0.90, 15, 0), 10);
            if (society) {
                this.type('1', society);
                break;
            }
            if (trytimes - 1 === i) throw "In SASC Image:-  ./login/menuprompt.png, ./sasc/society.png not found , Developer:- menu-" + menu + " society-" + society + " tries time:-" + i;
        }
        for (let i = 0; i < trytimes; i++) {
            accno = this.wait(this.getPattern(path.join(__dirname, "./sasc/accno.png"), 0.90, 0, 0), 10);
            if (accno) {
                this.type(param.accno, accno);
                break;
            }
            if (trytimes - 1 === i) throw "In SASC Image:-  ./sasc/accno.png not found , Developer:-  accno-" + accno + " tries time:-" + i;
        }
        //productselectalt 164,52        ------------------------------------
        this.keys("\t");
        this.keys("\b");
        this.type(param.accountcode);
        this.keys("\t");
        let productselectalt;
        let productselectalt1;
        for (let i = 0; i < trytimes; i++) {

            if (!productselectalt) productselectalt = this.wait(this.getPattern(path.join(__dirname, "./sasc/productselectalt.png"), 0.90, 164, 52), 3);
            if (!productselectalt1) productselectalt1 = this.wait(this.getPattern(path.join(__dirname, "./sasc/productselectalt1.png"), 0.90, 0, 0), 3);
            if (productselectalt) {
                let prdaltclick = this.click(productselectalt);
                if (prdaltclick) {
                    interest_control_code = this.wait(this.getPattern(path.join(__dirname, "./sasc/interestcontrolcode.png"), 0.90, 40, 0), 3);
                    if (interest_control_code) break;
                }
            } else if (productselectalt1) {
                let prdaltclick1 = this.click(productselectalt1);
                if (prdaltclick1) {
                    interest_control_code = this.wait(this.getPattern(path.join(__dirname, "./sasc/interestcontrolcode.png"), 0.90, 40, 0), 3);
                    if (interest_control_code) break;
                }
            }
            if (!productselectalt) break;

        }

        let interest_control_code;
        let interest_control_code1
        for (let i = 0; i < trytimes; i++) {
            if (!interest_control_code1) {
                interest_control_code = this.wait(this.getPattern(path.join(__dirname, "./sasc/interestcontrolcode.png"), 0.90, 40, 0), 3);
                this.click(interest_control_code);
                this.keys("\t");
            }
            interest_control_code1 = this.wait(this.getPattern(path.join(__dirname, "./sasc/interestcontrolcode.png"), 0.90, 40, 0), 3);
            if (!interest_control_code1) break;
            if (trytimes - 1 === i) throw "In SASC Image:-  ./sasc/interestcontrolcode.png not found , Developer:-  interest_control_code-" + interest_control_code + " tries time:-" + i;
        }
        let sascpagetwo1;
        let sascpagetwo;
        for (let i = 0; i < trytimes; i++) {
            if (!sascpagetwo1) {
                sascpagetwo = this.wait(this.getPattern(path.join(__dirname, "./sasc/sascpagetwo.png"), 0.94, 0, 0), 10);
                this.click(sascpagetwo);
            }
            sascpagetwo1 = this.wait(this.getPattern(path.join(__dirname, "./sasc/sascpagetwo.png"), 0.94, 0, 0), 3);
            if (!sascpagetwo1) break;
        }
        let sascoverrideevent = this.wait(this.getPattern(path.join(__dirname, "./sasc/sascoverrideevent.png"), 0.90, 55, 0), 5);
        this.type("NO", sascoverrideevent);
        let sasctranschanges = this.wait(this.getPattern(path.join(__dirname, "./sasc/sasctranschanges.png"), 0.90, 55, 0), 5);
        this.type("NO", sasctranschanges);
        let sascallowdds = this.wait(this.getPattern(path.join(__dirname, "./sasc/sascallowdds.png"), 0.90, 20, 0), 5);
        this.type("NO", sascallowdds);

        let paycode;
        for (let i = 0; i < trytimes; i++) {
            let page3 = this.wait(this.getPattern(path.join(__dirname, "./sasc/page3withoutalert.png"), 0.95, 0, 0), 10);
            this.click(page3);
            paycode = this.wait(this.getPattern(path.join(__dirname, "./sasc/paycode.png"), 0.90, 20, 0), 10);
            if (paycode) {
                break;
            }
            if (trytimes - 1 === i) throw "In SASC Image:-  ./sasc/page3withoutalert.png, ./sasc/paycode.png not found , Developer:-  page3-" + page3 + " paycode-" + paycode + " tries time:-" + i;
        }

        this.type('1', paycode);
        this.keys("\t");


        let sascnoticeind = this.wait(this.getPattern(path.join(__dirname, "./sasc/sascnoticeind.png"), 0.90, 31, 0), 10);
        this.type(param.noticeperiod.period, sascnoticeind)
        let sascnoticeperiod = this.wait(this.getPattern(path.join(__dirname, "./sasc/sascnoticeperiod.png"), 0.90, 0, 0), 10);
        this.type(param.noticeperiod.value, sascnoticeperiod)

        let finalpopup;
        let sascmenu;
        let sascsave;
        for (let i = 0; i < trytimes; i++) {
            let ok = this.wait(this.getPattern(path.join(__dirname, "./sasc/ok.png"), 0.90, 0, 0), 10);
            this.click(ok);
            // finalpopup = this.wait(this.getPattern(path.join(__dirname, "./sasc/finalpopup.png"), 0.85, 160, 50),5);
            sascsave = this.wait(this.getPattern(path.join(__dirname, "./sasc/sascsave.png"), 0.86, 0, 0), 5);
            if (finalpopup || sascsave) {
                break;
            }
            if (trytimes - 1 === i) throw "In SASC Image:-  ./sasc/ok.png, ./sasc/finalpopup.png, ./sasc/sascsave.png not found , Developer:- ok-" + ok + " finalpopup-" + finalpopup + " sascsave-" + sascsave + " tries time:-" + i;
        }
        for (let i = 0; i < trytimes; i++) {
            // finalpopup = this.wait(this.getPattern(path.join(__dirname, "./sasc/finalpopup.png"), 0.90, 160, 50), 5);
            // this.click(finalpopup);
            sascsave = this.wait(this.getPattern(path.join(__dirname, "./sasc/sascsave.png"), 0.80, 0, 0), 5);
            this.click(sascsave);
            sascmenu = this.wait(this.getPattern(path.join(__dirname, "./sasc/SASCmenu.png"), 0.90, 80, 0), 20);
            if (sascmenu) this.highlight(sascmenu);
            if (sascmenu) {
                this.click(sascmenu)
                // console.log("break final ");
                for (let x = 0; x <= 4; x++) {
                    this.keys("\b")
                }
                break;
            }
            if (trytimes - 1 === i) throw "In SASC Image:-  ./sasc/finalpopup.png, ./sasc/SASCmenu.png not found , Developer:-  finalpopup-" + finalpopup + " sascmenu-" + sascmenu + " tries time:-" + i;
        }
    }
    ACLM(param, isexisting = 0) {
        let gobtn;
        let menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.90, 0, 0), 200);
        this.type('ACLM', menu);
        this.keys("\n");
        this.keys("\n");

        if (isexisting === 0) {
            let search_surname;
            for (let i = 0; i < trytimes; i++) {
                let search_initial = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchinital.png"), 0.90, 0, 0), 10);
                if (search_initial) {
                    this.type(param.firstname.trim().charAt(0), search_initial);
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/searchinital.png not found , Developer:-  search_initial-" + search_initial + " tries time:-" + i;
            }
            for (let i = 0; i < trytimes; i++) {
                search_surname = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchsurname.png"), 0.90, 0, 0), 10);
                if (search_surname) {
                    this.type(param.lastname, search_surname);
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/searchsurname.png not found , Developer:-  search_surname-" + search_surname + " tries time:-" + i;
            }

            let search_dob = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchdob.png"), 0.90, 0, 0), 60);
            this.type(param.dateofbirth, search_dob);
            let newcust_alert;
            for (let i = 0; i < trytimes; i++) {
                let search_btn = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchbtn.png"), 0.90, 0, 0), 10);
                this.click(search_btn);
                newcust_alert = this.wait(this.getPattern(path.join(__dirname, "./aclm/newcustalert.png"), 0.90, 160, 51), 5);
                if (newcust_alert) {
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM its seems like account already exists , Developer:-  search_btn-" + search_btn + " newcust_alert-" + newcust_alert + " tries time:-" + i;
            }
            let createcustomer
            if (newcust_alert) {
                for (let i = 0; i < trytimes; i++) {
                    newcust_alert = this.wait(this.getPattern(path.join(__dirname, "./aclm/newcustalert.png"), 0.90, 160, 51), 10);
                    this.click(newcust_alert);
                    createcustomer = this.wait(this.getPattern(path.join(__dirname, "./aclm/createcustomer.png"), 0.90, 0, 0), 10);
                    if (createcustomer) {
                        break;
                    }
                    if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/newcustalert.png, ./aclm/createcustomer.png, not found , Developer:-  newcust_alert-" + newcust_alert + " createcustomer-" + createcustomer + " tries time:-" + i;
                }

            } else {
                createcustomer = this.wait(this.getPattern(path.join(__dirname, "./aclm/createcustomer.png"), 0.90, 0, 0), 60);
            }
            let society_no;
            for (let i = 0; i < trytimes; i++) {
                createcustomer = this.wait(this.getPattern(path.join(__dirname, "./aclm/createcustomer.png"), 0.90, 0, 0), 10);
                this.click(createcustomer);
                society_no = this.wait(this.getPattern(path.join(__dirname, "./aclm/society.png"), 0.90, 24, 0), 10);
                if (society_no) {
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/createcustomer.png, ./aclm/society.png, not found , Developer:-  createcustomer-" + createcustomer + " society-" + society + " tries time:-" + i;
            }

            this.type('1', society_no);
            let title = this.wait(this.getPattern(path.join(__dirname, "./aclm/title.png"), 0.90, 4, 0), 60);
            this.type(param.title, title);
            let initials = this.wait(this.getPattern(path.join(__dirname, "./aclm/initials.png"), 0.90, 0, 0), 60);
            this.type(param.firstname.trim().charAt(0).toUpperCase(), initials);
            let firstfname = this.wait(this.getPattern(path.join(__dirname, "./aclm/firstforname.png"), 0.90, 0, 0), 60);
            this.type(param.firstname, firstfname);
            let surname = this.wait(this.getPattern(path.join(__dirname, "./aclm/surname.png"), 0.90, 0, 0), 60);
            this.type(param.lastname, surname);
            this.keys("\t");
            this.keys("\t");
            let dob = this.wait(this.getPattern(path.join(__dirname, "./aclm/dob.png"), 0.90, 0, 0), 60);
            this.type(param.dateofbirth, dob);

            let mobileno = this.wait(this.getPattern(path.join(__dirname, "./aclm/mobileno.png"), 0.90, 0, 0), 60);
            this.type(param.mobileno, mobileno);
            let emailadd = this.wait(this.getPattern(path.join(__dirname, "./aclm/emailadd.png"), 0.90, 0, 0), 60);
            this.type(param.emailid, emailadd);
            let alreadyexsistpopup;
            let ok_cancel;
            let address_popup;
            let search_exisitingadd;
            for (let i = 0; i < trytimes; i++) {
                if (!alreadyexsistpopup && !address_popup) {
                    let apply = this.wait(this.getPattern(path.join(__dirname, "./aclm/apply.png"), 0.90, 0, 0), 10);
                    this.click(apply);
                }
                if (!alreadyexsistpopup) {
                    alreadyexsistpopup = this.wait(this.getPattern(path.join(__dirname, "./aclm/checkcustomerstatus.png"), 0.90, 0, 0), 10);
                }
                if (alreadyexsistpopup) {

                    if (!address_popup) {
                        ok_cancel = this.wait(this.getPattern(path.join(__dirname, "./aclm/okcancel.png"), 0.90, 30, 0), 10);
                        this.click(ok_cancel);

                    }
                    address_popup = this.wait(this.getPattern(path.join(__dirname, "./aclm/address.png"), 0.90, 90, 26), 10);

                    if (address_popup) {
                        this.click(address_popup);
                        search_exisitingadd = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchexisitingadd.png"), 0.90, 300, -10), 10);
                        if (search_exisitingadd) {
                            break;
                        }
                    }
                } else {
                    address_popup = this.wait(this.getPattern(path.join(__dirname, "./aclm/address.png"), 0.90, 90, 26), 10);
                    this.click(address_popup);
                    search_exisitingadd = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchexisitingadd.png"), 0.90, 300, -10), 10);
                    if (search_exisitingadd) {
                        break;
                    }
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/address.png, ./aclm/searchexisitingadd.png, not found , Developer:-  address_popup-" + address_popup + " search_exisitingadd-" + search_exisitingadd + " tries time:-" + i;
            }

            let address_postal;
            for (let i = 0; i < trytimes; i++) {
                search_exisitingadd = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchexisitingadd.png"), 0.90, 300, -10), 10);
                this.click(search_exisitingadd);
                address_postal = this.wait(this.getPattern(path.join(__dirname, "./aclm/posthouse.png"), 0.90, 0, 0), 10);
                if (address_postal) {
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/searchexisitingadd.png, ./aclm/posthouse.png, not found , Developer:-  search_exisitingadd-" + search_exisitingadd + " address_postal-" + address_postal + " tries time:-" + i;
            }

            this.type(param.address[0].postalcode, address_postal);
            let address_house = this.wait(this.getPattern(path.join(__dirname, "./aclm/houseno.png"), 0.90, 26, 0), 60);
            this.click(address_house)
            this.type(param.address[0].houseno, address_house);
            let none;
            for (let i = 0; i < trytimes; i++) {
                let address_search = this.wait(this.getPattern(path.join(__dirname, "./aclm/addsearch.png"), 0.90, -40, 0), 10);
                this.click(address_search);
                none = this.wait(this.getPattern(path.join(__dirname, "./newaccount/none.png"), 0.90, 0, 0), 10);
                if (none) {
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/addsearch.png, ./newaccount/none.png, not found , Developer:-  address_search-" + address_search + " none-" + none + " tries time:-" + i;
            }
            let ok;
            if (none) {
                none = this.wait(this.getPattern(path.join(__dirname, "./newaccount/none.png"), 0.90, 0, 0), 20);
                this.click(none);
                this.keys("u\"\ue002\"");
                for (let i = 0; i < trytimes; i++) {
                    let selectexistingadd = this.wait(this.getPattern(path.join(__dirname, "./newaccount/selectexistingadd.png"), 0.90, 0, 0), 10);
                    this.click(selectexistingadd);
                    ok = this.wait(this.getPattern(path.join(__dirname, "./aclm/okcancel.png"), 0.90, -35, 0), 10);
                    if (ok) {
                        break;
                    }
                    if (trytimes - 1 === i) throw "In ACLM Image:-  ./newaccount/selectexistingadd.png, ./aclm/okcancel.png, not found , Developer:-  selectexistingadd-" + selectexistingadd + " ok-" + ok + " tries time:-" + i;
                }

            } else {
                // console.log("add not found");
            }
            let okapplycancel;
            let warnignnino;
            let finalsaveclick1;
            for (let i = 0; i < trytimes; i++) {
                if (!okapplycancel && !finalsaveclick1) {
                    ok = this.wait(this.getPattern(path.join(__dirname, "./aclm/okcancel.png"), 0.90, -35, 0), 10);
                    this.click(ok);
                }

                okapplycancel = this.wait(this.getPattern(path.join(__dirname, "./aclm/okapplycancel.png"), 0.90, -75, -2), 10);
                finalsaveclick1 = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalsaveclick1.png"), 0.90, -75, -2), 10);
                if (okapplycancel || finalsaveclick1) {
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/okcancel.png, ./aclm/okapplycancel.png, ./aclm/finalsaveclick1.png not found , Developer:-  ok-" + ok + " okapplycancel-" + okapplycancel + " finalsaveclick1-" + finalsaveclick1 + " tries time:-" + i;
            }
            let nationalinsurancecode;
            for (let i = 0; i < trytimes; i++) {
                if (!warnignnino && !nationalinsurancecode) {
                    okapplycancel = this.wait(this.getPattern(path.join(__dirname, "./aclm/okapplycancel.png"), 0.90, -75, -2), 5);
                    finalsaveclick1 = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalsaveclick1.png"), 0.90, -75, -2), 5);
                    if (finalsaveclick1) this.click(finalsaveclick1);
                    else this.click(okapplycancel);
                }
                if (!warnignnino) {
                    warnignnino = this.wait(this.getPattern(path.join(__dirname, "./aclm/warnignnino.png"), 0.90, 70, 51), 10);
                }

                if (warnignnino) {
                    if (!nationalinsurancecode) {
                        warnignnino = this.wait(this.getPattern(path.join(__dirname, "./aclm/warnignnino.png"), 0.90, 70, 51), 10);
                        this.click(warnignnino);
                        nationalinsurancecode = this.wait(this.getPattern(path.join(__dirname, "./aclm/nationalinsurancecode.png"), 0.90, 0, 0), 10);
                    }

                    if (nationalinsurancecode) {
                        this.type(param.nationalinsuranceno, nationalinsurancecode);
                        break;
                    }
                } else {
                    nationalinsurancecode = this.wait(this.getPattern(path.join(__dirname, "./aclm/nationalinsurancecode.png"), 0.90, 0, 0), 10);
                    if (nationalinsurancecode) {
                        this.type(param.nationalinsuranceno, nationalinsurancecode);
                        break;
                    }
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/okapplycancel.png, ./aclm/finalsaveclick1.png, ./aclm/warnignnino.png, ./aclm/nationalinsurancecode.png  not found , Developer:-  okapplycancel-" + okapplycancel + " finalsaveclick1-" + finalsaveclick1 + " warnignnino-" + warnignnino + " nationalinsurancecode-" + nationalinsurancecode + " tries time:-" + i;
            }

            let customerID = ""
            for (let i = 0; i < trytimes; i++) {
                let customerIDpath = this.wait(this.getPattern(path.join(__dirname, "./aclm/customerID.png"), 0.85, 70, 0), 10);
                customerID = this.FindImageText(customerIDpath);
                if (parseInt(customerID)) break;
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/customerID.png, not found , Developer:-  customerIDpath-" + customerIDpath + " tries time:-" + i;
            }
            param.customersid = customerID.trim();


            let countryofbirth = this.wait(this.getPattern(path.join(__dirname, "./aclm/countryofbirth.png"), 0.90, 35, 0), 60);
            let countryType = param.country && param.country.length <= 4 ? param.country : "GB"

            this.type(countryType, countryofbirth);
            if (isexisting === 0) {
                let cumother;
                let cummobile;
                let found;
                for (let i = 0; i < trytimes; i++) {
                    let cumcheckmobileyes;
                    cumother = this.wait(this.getPattern(path.join(__dirname, "./aclm/cumother.png"), 0.90, 0, 0), 10);
                    this.click(cumother);
                    cummobile = this.wait(this.getPattern(path.join(__dirname, "./aclm/cummobile.png"), 0.90, 55, 0), 10);

                    if (!cummobile) cumcheckmobileyes = this.wait(this.getPattern(path.join(__dirname, "./aclm/cumcheckmobileyes.png"), 0.90, 0, 0), 10);
                    if (cummobile || cumcheckmobileyes) {
                        if (cumcheckmobileyes) found = true;
                        break;
                    }
                }
                if (!found) this.type("YES", cummobile);
            }


            for (let i = 0; i < trytimes; i++) {
                let okapplycancel1 = this.wait(this.getPattern(path.join(__dirname, "./aclm/okapplycancel.png"), 0.90, -75, -2), 10);
                let finalsaveclick1 = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalsaveclick1.png"), 0.90, -75, -2), 10);
                if (finalsaveclick1) this.click(finalsaveclick1);
                else this.click(okapplycancel1);
                gobtn = this.wait(this.getPattern(path.join(__dirname, "./aclm/gobtn.png"), 0.80, 0, 0));
                // gobtn = this.wait(path.join(__dirname, "./aclm/gobtn.png"));
                if (gobtn) {
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM Image:- ./aclm/okapplycancel.png, ./aclm/finalsaveclick1.png,./aclm/gobtn.png not found , Developer:-  okapplycancel1-" + okapplycancel1 + " finalsaveclick1-" + finalsaveclick1 + " tries time:-" + i;
            }
        } else {
            for (let i = 0; i < trytimes; i++) {
                let cusaccsearch = this.wait(this.getPattern(path.join(__dirname, "./aclm/cusaccsearch.png"), 0.90, 0, 0));
                // let searchaccno =  this.wait(this.getPattern(path.join(__dirname, "./aclm/searchaccno.png"), 0.90, 0, 0));
                let typeaccno = this.type(param.customersid, cusaccsearch);
                if (typeaccno) break;
                if (trytimes - 1 === i) throw "In ACLM Image:- ./aclm/cusaccsearch.png not found , Developer:-  cusaccsearch-" + cusaccsearch + " tries time:-" + i;
            }
            for (let i = 0; i < trytimes; i++) {
                let search_btn = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchbtn.png"), 0.90, 0, 0), 10);
                let searchclickacc = this.click(search_btn);
                gobtn = this.wait(this.getPattern(path.join(__dirname, "./aclm/gobtn.png"), 0.80, 0, 0));
                if (searchclickacc && gobtn) break;
                if (trytimes - 1 === i) throw "In ACLM Account Does Not Exists , Developer:-  gobtn-" + gobtn + " tries time:-" + i;
            }
        }
        let LINKACC
        for (let i = 0; i < trytimes; i++) {
            // gobtn = this.wait(path.join(__dirname, "./aclm/gobtn.png"));
            gobtn = this.wait(this.getPattern(path.join(__dirname, "./aclm/gobtn.png"), 0.80, 0, 0));
            let gobtnclick = this.click(gobtn);
            LINKACC = this.wait(this.getPattern(path.join(__dirname, "./aclm/LINKACC.png"), 0.90, 46, 0), 10);
            if (LINKACC) {
                break;
            }
            if (trytimes - 1 === i) throw "In ACLM Image:- ./aclm/gobtn.png not found , Developer:-  gobtn-" + gobtn + " tries time:-" + i;
        }
        let linknextfinish
        for (let i = 0; i < trytimes; i++) {
            LINKACC = this.wait(this.getPattern(path.join(__dirname, "./aclm/LINKACC.png"), 0.90, 46, 0), 10);
            this.click(LINKACC);
            linknextfinish = this.wait(this.getPattern(path.join(__dirname, "./aclm/linknextfinish.png"), 0.90, 0, 0), 10);
            if (linknextfinish) {
                break;
            }
            if (trytimes - 1 === i) throw "In ACLM Image:- ./aclm/LINKACC.png, ./aclm/linknextfinish.png not found , Developer:-  LINKACC-" + LINKACC + " linknextfinish-" + linknextfinish + " tries time:-" + i;
        }
        let searchacc;
        let findaccdetail;
        for (let i = 0; i < trytimes; i++) {
            if (!searchacc) {
                linknextfinish = this.wait(this.getPattern(path.join(__dirname, "./aclm/linknextfinish.png"), 0.90, 0, 0), 10);
                this.click(linknextfinish);
                searchacc = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchacc.png"), 0.90, 170, -3), 10);
            }

            if (searchacc) {
                if (!findaccdetail) {
                    searchacc = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchacc.png"), 0.90, 170, -3), 10);
                    this.click(searchacc);
                }
                findaccdetail = this.wait(this.getPattern(path.join(__dirname, "./aclm/findaccdetail.png"), 0.90, 0, 0), 10);
                if (findaccdetail) {
                    let accnolinkacc = this.wait(this.getPattern(path.join(__dirname, "./aclm/accnolinkacc.png"), 0.90, 0, 0), 10);
                    if (accnolinkacc) {
                        this.type(param.accno, accnolinkacc);
                        this.keys("\t");
                        break;
                    }
                }
            }
            if (trytimes - 1 === i) throw "In ACLM Image:- ./aclm/linknextfinish.png, ./aclm/searchacc.png, ./aclm/findaccdetail.png, ./aclm/accnolinkacc.png  not found , Developer:-  linknextfinish-" + linknextfinish + " searchacc-" + searchacc + " findaccdetail-" + findaccdetail + " accnolinkacc-" + accnolinkacc + " tries time:-" + i;
        }



        let linkcheckbox;
        for (let i = 0; i < trytimes; i++) {
            let goclearcancel = this.wait(this.getPattern(path.join(__dirname, "./aclm/goclearcancel.png"), 0.90, -60, 0), 10);
            this.click(goclearcancel);
            linkcheckbox = this.wait(this.getPattern(path.join(__dirname, "./aclm/linkcheckbox.png"), 0.90, -12, 9), 10);
            if (linkcheckbox) {
                break;
            }
            if (trytimes - 1 === i) throw "In ACLM Image:- ./aclm/goclearcancel.png, ./aclm/linkcheckbox.png not found , Developer:-  goclearcancel-" + goclearcancel + " linkcheckbox-" + linkcheckbox + " tries time:-" + i;
        }
        let previouslinkfinishacc;
        let clickall;
        for (let i = 0; i < trytimes; i++) {

            linkcheckbox = this.wait(this.getPattern(path.join(__dirname, "./aclm/linkcheckbox.png"), 0.90, -12, 9), 10);
            clickall = this.click(linkcheckbox);
            previouslinkfinishacc = this.wait(this.getPattern(path.join(__dirname, "./aclm/previouslinkfinishacc.png"), 0.90, 0, 0), 10);
            if (clickall && previouslinkfinishacc) {
                break;
            }
            if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/linkcheckbox.png , ./aclm/previouslinkfinishacc.png not found , Developer:-  linkcheckbox-" + linkcheckbox + " previouslinkfinishacc-" + previouslinkfinishacc + " tries time:-" + i;
        }

        let posacclink;
        let nextclick;
        for (let i = 0; i < trytimes; i++) {
            previouslinkfinishacc = this.wait(this.getPattern(path.join(__dirname, "./aclm/previouslinkfinishacc.png"), 0.90, 0, 0), 10);
            nextclick = this.click(previouslinkfinishacc);
            posacclink = this.wait(this.getPattern(path.join(__dirname, "./aclm/posacclink.png"), 0.90, 5, 12), 10);
            if (posacclink) {
                break;
            }

            if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/previouslinkfinishacc.png, ./aclm/posacclink.png not found , Developer:-  previouslinkfinishacc-" + previouslinkfinishacc + " posacclink-" + posacclink + " tries time:-" + i;

        }

        this.type('OB', posacclink);
        let errorokagenotallow = this.wait(this.getPattern(path.join(__dirname, "./aclm/errorok.png"), 0.90, 0, 0), 5);
        if (errorokagenotallow) {
            throw " customers age is less than minimum allowed for account type "
        }

        let salutionupdate = this.wait(this.getPattern(path.join(__dirname, "./aclm/salutionupdate.png"), 0.90, 118, 50), 20);
        let finalnext;
        if (salutionupdate) {
            for (let i = 0; i < trytimes; i++) {
                salutionupdate = this.wait(this.getPattern(path.join(__dirname, "./aclm/salutionupdate.png"), 0.90, 118, 50), 10);
                this.click(salutionupdate);
                finalnext = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalnext.png"), 0.90, 0, 0), 10);
                if (finalnext) {
                    break;
                }
                if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/salutionupdate.png, ./aclm/finalnext.png not found , Developer:-  salutionupdate-" + salutionupdate + " finalnext-" + finalnext + " tries time:-" + i;

            }

        } else {
            finalnext = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalnext.png"), 0.90, 0, 0), 60);
        }

        let finalsavealert;
        let finalfinish;
        let finalcancel;
        let eidcheck;
        let eidok;
        for (let i = 0; i < trytimes; i++) {
            if (!finalsavealert && !finalfinish) {
                finalnext = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalnext.png"), 0.90, 0, 0), 10);
                this.click(finalnext);
            }
            if (!finalsavealert) {
                finalsavealert = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalsavealert.png"), 0.90, 175, 49), 5);
            }

            //  finalfinish= this.wait(this.getPattern(path.join(__dirname, "./aclm/finalfinish.png"),0.90,75,0));
            if (finalsavealert) {
                if (!finalfinish) {
                    finalsavealert = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalsavealert.png"), 0.90, 175, 49), 10);
                    this.click(finalsavealert);
                    finalfinish = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalfinish.png"), 0.90, 75, 0), 10);
                }

                if (finalfinish) {
                    this.click(finalfinish);
                    eidcheck = this.wait(this.getPattern(path.join(__dirname, "./aclm/eidcheck.png"), 0.85, 0, 0), 10);
                    eidok = this.wait(this.getPattern(path.join(__dirname, "./aclm/okeidcheck.png"), 0.85, 0, 0), 10);
                    if (eidcheck && eidok) this.click(eidok);
                    // add eid check over here
                    finalcancel = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalcancel.png"), 0.90, 0, 0), 10);
                    if (finalcancel) {
                        break;
                    }
                }

            } else {
                if ((!eidcheck && !eidok && !finalfinish) || (!finalfinish && !finalcancel)) {
                    finalfinish = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalfinish.png"), 0.90, 75, 0), 10);
                    this.click(finalfinish);
                }

                eidcheck = this.wait(this.getPattern(path.join(__dirname, "./aclm/eidcheck.png"), 0.85, 0, 0), 10);
                if (eidcheck) eidok = this.wait(this.getPattern(path.join(__dirname, "./aclm/okeidcheck.png"), 0.85, 0, 0), 5);
                if (eidcheck && eidok) this.click(eidok);
                // add eid check over here
                finalcancel = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalcancel.png"), 0.90, 0, 0), 10);
                if (finalcancel) {
                    break;
                }
            }
            if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/finalfinish.png, ./aclm/eidcheck.png, ./aclm/okeidcheck.png, ./aclm/finalcancel.png not found , Developer:-  finalfinish-" + finalfinish + " eidcheck-" + eidcheck + " eidok-" + eidok + " finalcancel" + finalcancel + " tries time:-" + i;
        }

        /**error handle if occur don't delete this line */
        //     let finalerralert;
        //     for(let i=0;i<5;i++){
        //        this.click(finalsavealert);
        //        finalerralert =this.wait(this.getPattern(path.join(__dirname, "./aclm/finalerralert.png"),0.90,173,50));
        //        if(finalerralert){
        //            break;
        //        }
        //     }

        /**error handle if occur don't delete this line */

        let menuaclm;
        for (let i = 0; i < trytimes; i++) {

            finalcancel = this.wait(this.getPattern(path.join(__dirname, "./aclm/finalcancel.png"), 0.90, 0, 0), 10);
            this.click(finalcancel);
            menuaclm = this.wait(this.getPattern(path.join(__dirname, "./aclm/menuaclm.png"), 0.90, 80, 0), 10);
            if (menuaclm) {
                this.click(menuaclm);
                for (let x = 0; x <= 4; x++) {
                    this.keys("\b");
                }
                break;
            }
            if (trytimes - 1 === i) throw "In ACLM Image:-  ./aclm/finalcancel.png, ./aclm/menuaclm.png not found , Developer:-  finalcancel-" + finalcancel + " menuaclm-" + menuaclm + " tries time:-" + i;
        }



    }
    ACV(param) {
        let menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.90, 0, 0), 200);
        this.type('ACV', menu);
        this.keys("\n");
        this.keys("\n");
        let accno;

        for (let i = 0; i < trytimes; i++) {
            let society = this.wait(this.getPattern(path.join(__dirname, "./acv/society.png"), 0.90, 0, 0), 10);
            if (society) {
                this.type("1", society);
                break;
            }
            if (trytimes - 1 === i) throw "In ACV Image:-  ./acv/society.png  not found , Developer:-  society-" + society + " tries time:-" + i;
        }
        for (let i = 0; i < trytimes; i++) {
            accno = this.wait(this.getPattern(path.join(__dirname, "./acv/accno.png"), 0.90, 0, 0), 10);
            if (accno) {
                this.type(param.accno, accno);
                break;
            }
            if (trytimes - 1 === i) throw "In ACV Image:- ./acv/accno.png  not found , Developer:-  accno-" + accno + " tries time:-" + i;
        }

        this.keys("\t");
        let validate;
        let acvmenu;
        let success;
        let acvinconsistencies;
        let acvokclick;
        for (let i = 0; i < trytimes; i++) {
            validate = this.wait(this.getPattern(path.join(__dirname, "./acv/validate.png"), 0.85, 0, 0), 10);
            this.click(validate);
            // success = this.wait(this.getPattern(path.join(__dirname, "./acv/success.png"), 0.85, 140, 50), 10);
            // if (!success) acvinconsistencies = this.wait(this.getPattern(path.join(__dirname, "./acv/acvinconsistencies.png"), 0.85, 176, 56), 5);
            // if (!acvinconsistencies)
            acvokclick = this.wait(this.getPattern(path.join(__dirname, "./acv/acvokclick.png"), 0.85, 0, 0), 10);
            if (acvokclick) {
                break;
            }
            if (trytimes - 1 === i) throw "In ACV Image:- ./acv/validate.png ,./acv/acvokclick.png not found ,  Developer:-  validate-" + validate + " success-" + success + " tries time:-" + i;
        }

        let successclose;
        for (let i = 0; i < trytimes; i++) {
            // if (!acvokclick) {
            //     success = this.wait(this.getPattern(path.join(__dirname, "./acv/success.png"), 0.85, 140, 50), 10);
            //     this.click(success);
            // }
            // if (!success) {
            acvokclick = this.wait(this.getPattern(path.join(__dirname, "./acv/acvokclick.png"), 0.85, 0, 0), 10);
            this.click(acvokclick);
            // }
            if (acvokclick) successclose = this.wait(this.getPattern(path.join(__dirname, "./acv/successclose.png"), 0.85, 0, 0), 10);
            if (successclose) {
                break;
            }
            if (trytimes - 1 === i) throw "In ACV Image:- ./acv/success.png ,./acv/successclose.png not found , Developer:-  success-" + success + " successclose-" + successclose + " tries time:-" + i;
        }

        for (let i = 0; i < trytimes; i++) {
            successclose = this.wait(this.getPattern(path.join(__dirname, "./acv/successclose.png"), 0.85, 0, 0), 10);
            this.click(successclose);
            acvmenu = this.wait(this.getPattern(path.join(__dirname, "./acv/menuacv.png"), 0.85, 80, 0), 10);
            if (acvmenu) {
                this.click(acvmenu);
                for (let x = 0; x <= 4; x++) {
                    this.keys("\b");
                }
                break;
            }
            if (trytimes - 1 === i) throw "In ACV Image:- ./acv/successclose.png ,./acv/menuacv.png not found , Developer:-  successclose-" + successclose + " acvmenu-" + acvmenu + " tries time:-" + i;
        }
    }
    updateExsting(params, customerid) {
        try {
            let menu = this.wait(this.getPattern(path.join(__dirname, "./login/menuprompt.png"), 0.90, 0, 0), 60);
            this.type('CUM', menu);
            this.keys("\n");
            this.keys("\n");
            for (let i = 0; i < trytimes; i++) {
                let cusaccsearch = this.wait(this.getPattern(path.join(__dirname, "./aclm/cusaccsearch.png"), 0.90, 0, 0));
                // let searchaccno =  this.wait(this.getPattern(path.join(__dirname, "./aclm/searchaccno.png"), 0.90, 0, 0));
                let typeaccno = this.type(customerid, cusaccsearch);
                if (typeaccno) break;
                if (trytimes - 1 === i) throw "In CUM(existing update) Image:- ./aclm/cusaccsearch.png not found , Developer:-  cusaccsearch-" + cusaccsearch + " tries time:-" + i;
            }
            let gobtn;
            for (let i = 0; i < trytimes; i++) {
                let search_btn = this.wait(this.getPattern(path.join(__dirname, "./aclm/searchbtn.png"), 0.90, 0, 0), 10);
                let searchclickacc = this.click(search_btn);
                gobtn = this.wait(this.getPattern(path.join(__dirname, "./aclm/gobtn.png"), 0.80, 0, 0));
                if (gobtn) break;
                if (trytimes - 1 === i) throw "In CUM(existing update) Image:- ./aclm/gobtn.png, ./aclm/searchbtn.png, Developer:-  gobtn-" + gobtn + " tries time:-" + i;
            }
            let emailupdate;
            let emptyemail;
            for (let i = 0; i < trytimes; i++) {
                gobtn = this.wait(this.getPattern(path.join(__dirname, "./aclm/gobtn.png"), 0.80, 0, 0));
                this.click(gobtn);
                emailupdate = this.wait(this.getPattern(path.join(__dirname, "./cum/emailupdate.png"), 0.88, 54, 0), 10);
                if (emailupdate) break;
            }
            if (params.emailid) {
                for (let i = 0; i < trytimes; i++) {
                    emailupdate = this.wait(this.getPattern(path.join(__dirname, "./cum/emailupdate.png"), 0.88, 54, 0), 10);
                    this.click(emailupdate);
                    this.doubleClick(emailupdate);
                    this.doubleClick(emailupdate);
                    this.keys("\b");
                    emptyemail = this.wait(this.getPattern(path.join(__dirname, "./cum/emptyemail.png"), 0.88, 0, 0), 3);
                    if (emptyemail) break;
                    if (trytimes - 1 === i) throw "err in find image";
                }
                this.type(params.emailid, emptyemail);
            }

            if (params.nationalinsuranceno) {
                let cumpersonal;
                let nicodecum;
                let emptynicode;
                for (let i = 0; i < trytimes; i++) {
                    if (!cumpersonal) cumpersonal = this.wait(this.getPattern(path.join(__dirname, "./cum/cumpersonal.png"), 0.88, 0, 0), 10);
                    this.click(cumpersonal);
                    nicodecum = this.wait(this.getPattern(path.join(__dirname, "./cum/nicodecum.png"), 0.88, 40, 0), 3);
                    if (nicodecum) break;
                    if (trytimes - 1 === i) throw "err in find image";
                }
                for (let i = 0; i < trytimes; i++) {
                    this.click(nicodecum);
                    this.doubleClick(nicodecum);
                    this.doubleClick(nicodecum);
                    this.keys("\b");
                    emptynicode = this.wait(this.getPattern(path.join(__dirname, "./cum/emptynicode.png"), 0.88, 0, 0), 3);
                    if (emptynicode) break;
                    if (trytimes - 1 === i) throw "err in find image";
                }
                this.type(params.nationalinsuranceno, emptynicode);
                this.keys("\t");
                let cumerrormsg1;
                let cumerrormsg = this.wait(this.getPattern(path.join(__dirname, "./cum/cumerrormsg.png"), 0.88, 0, 0), 3);
                if (cumerrormsg) {
                    for (let i = 0; i < trytimes; i++) {
                        cumerrormsg1 = this.wait(this.getPattern(path.join(__dirname, "./cum/cumerrormsg.png"), 0.88, 0, 0), 3);
                        this.click(cumerrormsg1);
                        cumerrormsg1 = this.wait(this.getPattern(path.join(__dirname, "./cum/cumerrormsg.png"), 0.88, 0, 0), 3);
                        if (!cumerrormsg1) break;
                        if (trytimes - 1 === i) throw "err in find image";
                    }
                    for (let i = 0; i < trytimes; i++) {
                        this.keys("\b");
                        emptynicode = this.wait(this.getPattern(path.join(__dirname, "./cum/emptynicode.png"), 0.88, 0, 0), 3);
                        if (emptynicode) break;
                        else {
                            this.click(nicodecum);
                            this.doubleClick(nicodecum);
                            this.keys("\b");
                            emptynicode = this.wait(this.getPattern(path.join(__dirname, "./cum/emptynicode.png"), 0.88, 0, 0), 3);
                            if (emptynicode) break;
                        }
                        if (trytimes - 1 === i) throw "err in find image";
                    }
                    params.message_ninumber = "failed to add"
                }
            }
            let cumokupdate;
            let cumok;
            let cummenu;
            for (let i = 0; i < trytimes; i++) {
                if (!cumok) {
                    cumokupdate = this.wait(this.getPattern(path.join(__dirname, "./cum/cumokupdate.png"), 0.88, -95, 0), 3);
                    this.click(cumokupdate);
                }
                cumok = this.wait(this.getPattern(path.join(__dirname, "./cum/cumok.png"), 0.88, 30, 0), 3);
                if (cumok) {
                    this.click(cumok);
                    cummenu = this.wait(this.getPattern(path.join(__dirname, "./cum/cummenu.png"), 0.88, 35, 0), 3);
                    if (cummenu) {
                        for (let x = 0; x < 6; x++) {
                            this.keys("\b");
                        }
                        break;
                    }

                } else {
                    cummenu = this.wait(this.getPattern(path.join(__dirname, "./cum/cummenu.png"), 0.88, 35, 0), 3);
                    if (cummenu) {
                        for (let x = 0; x < 6; x++) {
                            this.keys("\b");
                        }
                        break;
                    }
                }

                if (trytimes - 1 === i) throw "err in find image";
            }


        } catch (err) {
            // console.log(err)
            try {
                let datetime = new Date().getTime();
                params.img = err.replace(/\s/g, '').substr(0, 6) + datetime;
                this.capture(params.img);
            } catch (e) { }
            this.hardClose("iexplore.exe");
            loginsopra.ensureSopraScreen();
            return true;
        }


    }

}
// const na = new NewAccount();
// na.execute({});
module.exports = NewAccount;
// process.exit(0);
