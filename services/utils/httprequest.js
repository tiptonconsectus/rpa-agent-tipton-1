const http = require("http");
const FormData = require("form-data");
const fs_file=require("fs");

class HTTPRequest {
    constructor(){

    }
    HttpGet(options){
        return   new Promise((resolve, reject) => {
            const req = http.request(options, (res) => {
                res.setEncoding('utf8');
                let data;
                let error= (res.statusCode !== 200);
         
                res.on('data', (chunk) => {                  
                    try {
                        console.log(chunk)
                     data=   JSON.parse(chunk);
                    } catch (e) {
                        console.error("e.message");
                        console.error(e);
                        reject("error in try method")
                    }
                    // resolve(data);
                });
                res.on('end', () => {
                    // debug('No more data in response.');
                    if(error){                       
                        reject(data)
                     }else{
                      resolve(data)
                     }     

                });
            })
                req.on('error', (e) => {
                    console.error(`Got error: ${e.message}`);
                    /** connect issue handle here */
               //     debugerror(`problem with request: ${e.message}`);
                    reject(e);
                });
           
            // write data to request body
           // req.write(postData);
            req.end();

        })

    }
    HttpPost(options,postData){
        return   new Promise((resolve, reject) => {
            const req = http.request(options, (res) => {
              //  const { statusCode } = res;
                let data;
                let error= (res.statusCode !== 200);
                // if (statusCode !== 200) {
                //    error = new Error('Request Failed.\n' +
                //                      `Status Code: ${statusCode}`);
                //  } 
               //   else if (!/^application\/json/.test(contentType)) {
               //     error = new Error('Invalid content-type.\n' +
               //                       `Expected application/json but received ${contentType}`);
               //   }
                //  if (error) {
                //    console.error(error.message);
                //    console.error("error.message");
                //    // consume response data to free up memory
                //    reject("error on agent response");
                //    res.resume();
                //    return;
                //  }
                
                res.setEncoding('utf8');
               
                res.on('data', (chunk) => {
                    try {
                        console.log(chunk)
                     data=   JSON.parse(chunk);
                    } catch (e) {
                        console.error("e.message");
                        console.error(e.message);
                        reject("error in try method")
                    }
                //   data += chunk;
                   //use here resolve
                  
               });
               res.on('end', () => {
                 
                      // const parsedData = data;
                      // const parsedData = data;
                      if(error){                       
                         reject(data)
                      }else{
                       resolve(data)
                      }         
                   // debug('No more data in response.');
               });
          
            
        })
            req.on('error', (e) => {
            console.log("e")
                console.log(e)
               // debugerror(`problem with request: ${e.message}`);
                reject(e);
            });
            // req.setTimeout(1000, function () {
            // reject("timeout");
            // req.abort();
            // });
            // write data to request body
        
            req.write(postData);
            req.end();
            
        })
    }
    HttpPostForm(req,result,oauth,zip=null){
        console.log(req.body)
        return   new Promise((resolve, reject) => {
            let formData = new FormData();
            formData.append("name", req.body.name);
            formData.append("type", req.body.type);
            formData.append("queued", req.body.queued);
            if(req.body.version!==undefined && req.body.version!==null) formData.append("version", req.body.version);
            if (req.body.id) formData.append("id", req.body.id);
           // if(zip===null){
              formData.append("action-path-zip", fs_file.createReadStream(req.file.path));
           //  }
          //  else{
          //     formData.append("action-path-zip", fs_file.createReadStream(zip)); 
          //  }
            formData.submit({
                host: result.host,
                port: result.port,
                method: "POST",
                path: "/tasks/upload-action-path",
                headers: {
                    'Authorization': 'Bearer ' + oauth,
                }
            }, function (err, res) {
                if (err){
                   // debug(err);
                   console.log("err in http")
                   console.log(err)
                    reject(err);
                } else {
                   let errorcode = (res.statusCode !== 200);
                   var body = "";
                    res.setEncoding('utf8');
                    res.on('data', function (chunk) {
                        body += chunk;
                    });
                    res.on('error', function (err) {
                    //    debug(`Error: statusCode ${res.statusCode}`);
                    //    debug(err);
                    console.log("res err")
                    console.log(err)
                        reject(err);
                    });
                    res.on('end', function () {
                        if(errorcode==true){
                            reject(body);
                        }else{
                        resolve(body);
                        }
                    });
                }
            });
            
        })
    }
}
module.exports=HTTPRequest;