let mailModel = require("./mail_model");
let mailObject = new mailModel();

module.exports = (router) => {
    router.post("/sendmail", function (req, res) {
        mailObject.sendMail(req, res)
    })

    return router
}