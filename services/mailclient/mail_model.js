"use strict"
class MailModel {
    constructor(){

    }

    sendMail(req,res){
        const mailTo = req.body.mailto;
        const subject = req.body.subject;
        const mailbody = req.body.message;
        const config = require("../../config/config")
        const fs = require("fs")
        const nodemailer = require("nodemailer")
        let transporter = nodemailer.createTransport({
            host: config.nodemailer.smtp,
            port: config.nodemailer.port,
            secure: true, // true for 465, false for other ports
            auth: {
                user: config.nodemailer.email,
                pass: config.nodemailer.passwd 
            }
        });
    
        // setup email data with unicode symbols
        let mailOptions = {
            from: config.nodemailer.email,
            to: mailTo, 
            subject: subject, 
            text: 'Hello world?',
            html: mailbody, 
            attachments: [{   // stream as an attachment
                filename: 'logo.png',
                content: fs.createReadStream('F:/consectus-api/consectus-api/assets/email/logo.png')
            }]
        };
    
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                // console.log(error);
                res.status(300).send({"status":false,"message":"Mail Sending Failed","error":error.message})
            }
            // console.log('Message sent: %s', info.messageId);
            // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            res.status(300).send({"status":true,"message":"Mail Send Successfully"})
        });
        
    }
}

module.exports = MailModel