"use strict"
class SMSModel {
    constructor() {

    }

    sendSMS(req, res) {
        const smsbody = req.body.message;
        const config = require("../../config/config")
        const client = require('twilio')(config.twilio.accountsid, config.twilio.authToken)

        client.messages
            .create({
                body: smsbody,
                to: '+447980493084',
                from: '+441202286227'
            })
            .then(message => console.log("done", message.sid))
            .catch(err => console.log(err))

        res.status(300).send({ "status": true, "message": "Sms Send Successfully" })
    }


    sendSMS_2(mobileno, text) {
        return { "status": true, "message": "Sms Send Successfully" };
    }
}

module.exports = SMSModel