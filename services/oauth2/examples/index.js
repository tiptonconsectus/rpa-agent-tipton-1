crypto = require('crypto');
http = require("http");
querystring = require("querystring");
class OauthToken {
    constructor() {
        this.tokenExpiry = new Date().getTime();
        this.verifier_code = null;
        this.code_challenge = null;
        this.oauth2Host = "localhost";
        this.oauth2Port = "9090";
        this.code = null;
        this.token = null;
        this.state = null;
        this.expires_in = null;
    }
    async getAccessToken(fresh = false) {
            //check if token exists or if token expiry has not reached or token is requested fresh
            if (this.token && !fresh && this.tokenExpiry > (new Date().getTime()+5000)) {
                resolve(this.token);
            } else {
                //get fresh token
                try {
                    this.code = await this.getAuthorizationCode();
                    // console.log("Authorization Code: "+this.code)
                    this.token = await this.getToken();
                    this.tokenExpiry = new Date().getTime()+(this.token.expires_in*1000);
                    // console.log("Token: "+this.token);
                } catch (error) {
                    this.code = null;
                    this.token = null;
                    throw error;
                }
            }
    }
    verify() {
        let self = this;
        self.getAccessToken(true).then(()=>{
            return new Promise(function (resolve, reject) {
                const postData = querystring.stringify({
                    test: 'consectus-rpa'
                });
                const options = {
                    hostname: self.oath2Host,
                    port: self.oauth2Port,
                    path: '/oauth2/verify',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + self.token,
                        'Content-Length': Buffer.byteLength(postData)
                    }
                };
                const req = http.request(options, (res) => {
                    res.setEncoding('utf8');
                    res.on('data', (chunk) => {
                        let data = JSON.parse(chunk);
                        // console.log(JSON.stringify(data, false, 4));
                        resolve(data.token);
                    });
                    res.on('end', () => {
                        // console.log('No more data in response.');
                    });
                });
                req.on('error', (e) => {
                    console.error(`problem with request: ${e.message}`);
                    reject(e);
                });
                // write data to request body
                req.write(postData);
                req.end();
            });        

        }).catch((err)=>{
            // console.log(err);
        })
    }
    /**
     * Get access token by passing authorization code
     */
    async getToken() {
        let self = this;
        return new Promise(function (resolve, reject) {
            const postData = querystring.stringify({
                state: self.state,
                code: self.code,
                verifier_code: self.verifier_code,
                grant_type: "authorization_code",
                code_challenge_method: "S256",
                client_id: 'consectus-rpa'
            });
            // console.log(postData);
            const options = {
                hostname: self.oauth2Host,
                port: self.oauth2Port,
                path: '/oauth2/token',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(postData)
                }
            };
            // {
            //     "code": "c411c047-7d12-4d51-a354-8fed9f6ae6d9",
            //     "client_id": "consectus-rpa",
            //     "access_token": "fcb43f24-7072-4089-bd39-75284379a646",
            //     "expires_in": "7200"
            // }
            const req = http.request(options, (res) => {
                // console.log(`STATUS: ${res.statusCode}`);
                // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    let data = JSON.parse(chunk);
                    // console.log(`BODY: ${JSON.stringify(data, false, 4)}`);
                    // console.log(data.access_token);
                    self.token = data.access_token;
                    self.expires_in = data.expires_in;
                    setTimeout(() => {
                        //request for accessToken in expires_in - 2 seconds;
                        self.token = null;
                        self.getAccessToken(true);
                    }, parseInt(data.expires_in) + 2000);
                    // console.log("Refreshing Token in "+(data.expires_in-2000)+" seconds")
                    resolve(self.token);
                });
                res.on('end', () => {
                    // console.log('No more data in response.');
                });
            });
            req.on('error', (e) => {
                console.error(`problem with request: ${e.message}`);
                reject(e);
            });
            // write data to request body
            req.write(postData);
            req.end();
        });
    }    
    async getAuthorizationCode() {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.verifier_code = getVerifierCode();
            self.code_challenge = getCodeChallenge(self.verifier_code);
            self.state = crypto.randomBytes(32) + ""; // some random string useful to verify when token is requested.
            const postData = querystring.stringify({
                state: self.state,
                code_challenge: self.code_challenge,
                response_type: "code",
                code_challenge_method: "S256",
                client_id: 'consectus-rpa'
            });
            const options = {
                hostname: self.oauth2Host,
                port: self.oauth2Port,
                path: '/oauth2/authorization_code',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(postData)
                }
            };
            // {
            //     "state": "abcd",
            //     "code": "af42b25f-d863-4501-a3b1-613a13b7c24c"
            // }
            const req = http.request(options, (res) => {
                // console.log(`STATUS: ${res.statusCode}`);
                // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    let data = JSON.parse(chunk);
                    // console.log(`BODY: ${JSON.stringify(data, false, 4)}`);
                    resolve(data.code);
                });
                res.on('end', () => {
                    // console.log('No more data in response.');
                });
            });
            req.on('error', (e) => {
                console.error(`problem with request: ${e.message}`);
                reject(e);
            });
            // write data to request body
            req.write(postData);
            req.end();
        });
    }
}
function base64URLEncode(code) {
    return code.toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
}
function getVerifierCode() {
    const code = crypto.randomBytes(32);
    const verifier_code = base64URLEncode(code);
    return verifier_code;
}
function getCodeChallenge(code) {
    return base64URLEncode(crypto.createHash('sha256').update(code).digest());
}

let example = new OauthToken();
example.verify();
setTimeout(() => {
    // console.log("Calling verify again");
    example.verify();
}, 8000);


