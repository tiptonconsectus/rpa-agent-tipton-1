let port = process.env.PORT || 9089;
let database = process.env.DATABASE || 'consectus_rpa_agent';
let database_user = process.env.DATABASE_USER || 'consectus_rpa_agent';
let database_password = process.env.DATABASE_PASSWORD || 'ITISSAFE';
let database_host = process.env.DATABASE_HOST || 'localhost';
let database_port = process.env.DATABASE_PORT || "3306";

module.exports = {
    'oauthPort': port,
    'secret': 'xyz-abc-def',
    'defaultnamespace': "cf30291d-76d6-4ec1-8569-d70f5148bad4",
    'expiresIn': '7200000',
    'codeLifetimeSeconds': 300, // -1 for unlimited lifetime else token request must come within the given time
    "database": {
        "host": database_host,
        "user": database_user,
        "password": database_password,
        "port": database_port,
        "database": database
    },
    "tables": {
        clients: "oauth2_clients",
        tokens: "oauth2_access_tokens",
        codes: "oauth2_authorization_codes"
    },
    "clients": [
        {
            client_id: "consectus-rpa", name: "Consectus RPA"
        },
        {
            client_id: "consectus-api", name: "Consectus API"
        }
    ]
}