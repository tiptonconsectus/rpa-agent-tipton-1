/**
 * 
 app.use('/execute', function (req, res, next) {
  var Pattern = java.import("org.sikuli.script.Pattern");
  var Screen = java.import("org.sikuli.script.Screen");
  var Match = java.import("org.sikuli.script.Match");
  var ScreenImage = java.import("org.sikuli.script.ScreenImage");
  var screen = new Screen();
  // console.log(screen.toString());
  // var scr = screen.captureSync();
  // console.log(scr.getFilenameSync());
  try {
    let image = __dirname + "/resources/mac-search.png";
    var p = new Pattern(image);
    console.log(image)
    console.log(p);
    // var p = new Pattern(__dirname + "/bin/resources/ubuntu-activity-button.png");
    var x = p.similarSync(0.90);
    var m = screen.existsSync(x);
    var s = m.getScoreSync();
    console.log(s);
    m.clickSync();
    screen.typeSync("Hello");
  } catch (error) {
    console.error(error);
  }
  res.render('index', { title: 'Express' });
});
 */
const exec = require('child_process');
const fs = require('fs');
const path = require('path');
const config = require("../../config/config");
class ActionPath {
    constructor(req = {}, res = {}) {
        this.error = null;
        this.app = req.app;
        this.req = req;
        this.res = res;
        this.config = config;
        if (req.app !== undefined) {
            this.debug = req.app.debug;
            this.debugerror = req.app.debugerror;
            this.java = req.app.java;
            this.screen = this.app.screen;

            // this.setting = this.java.newInstanceSync('org.sikuli.script.Settings');
            // this.setting.ActionLogs = false;
            // this.setting.InfoLogs = false;
            // Settings.ActionLogs = false; // messages from click, ...
            // Settings.InfoLogs = false; //other information messages
            if (this.screen === undefined || this.screen === null)
                this.screen = this.getScreen();
        } else {
            this.debug = require("debug")("actionpath:");
            this.debugerror = require("debug")("actionpath-error:");
            // initialize Java
            const javaInit = require("../../bin/javaInit");
            //need the below environment variable to work on Macosx
            process.env['JAVA_STARTED_ON_FIRST_THREAD_' + process.pid] = '1'
            /* java instance */
            this.java = javaInit.getJavaInstance();
            this.screen = this.getScreen();
        }
    }
    /**
     * Must provide atleast imageTL & imageBR.
     * If more than matches are found, the nearest to TL is used.
     * If additional TR or BL are provided then they are used as a guide to find nearest matching BL
     * @param {filename} imageTL 
     * @param {filename} imageBR 
     * @param {filename} imageTR 
     * @param {filename} imageBL 
     */
    getRegion(image = null, imageTL = null, imageBR = null, imageTR = null, imageBL = null) {
        try {
            if (image != null) {
                let rect = java.newInstanceSync("java.awt.Rectangle", image.x, image.y, image.w, image.h);
                let region = java.callStaticMethodSync("org.sikuli.script.Region", "create", rect);
                return region;
            }
        } catch (error) {
            return null;
        }
    }
    regionClick(region, pattern) {
        try {
            region.clickSync(pattern);
            return true;
        } catch (error) {
            return null;
        }
    }

    SetRegion(x = null, y = null, h = null, w = null) {
        try {
            const Region = this.java.newInstanceSync("org.sikuli.script.Region");
            Region(x, y, h, w);
            // region.findSync(image);
            return true;

        } catch (error) {
            return null;
        }
    }
    getPattern(filename, similar, offsetX, offsetY) {
        try {
            let p;
            if (similar !== undefined)
                p = this.java.newInstanceSync("org.sikuli.script.Pattern", filename).similarSync(similar);
            else
                p = this.java.newInstanceSync("org.sikuli.script.Pattern", filename).similarSync(0.90);
            if (offsetX !== undefined && offsetY !== undefined) return p.targetOffsetSync(offsetX, offsetY);
            else return p;
        } catch (error) {
          //  console.log("patter errpr", error)
            this.error = "getPattern failed for: " + filename + ' ' + error;
            return null;
        }
    }

    getScreen(id = null) {
        try {
            if (!id) {
                this.screen = this.java.newInstanceSync("org.sikuli.script.Screen");
            } else {
                this.screen = this.java.newInstanceSync("org.sikuli.script.Screen", id);
            }
            return this.screen;
        } catch (error) {
            this.screen = null;
            if (id === undefined)
                this.error = "getScreen Faiiled " + + ' ' + error
            else
                this.error = "getScreen Faiiled for: " + id + ' ' + error;
            return null;
        }
    }
    find(filename, similar, offsetX, offsetY) {
        try {
            let pattern = null;
            if (similar !== undefined)
                pattern = this.java.newInstanceSync("org.sikuli.script.Pattern", filename).similarSync(similar);
            else
                pattern = this.java.newInstanceSync("org.sikuli.script.Pattern", filename).similarSync(0.90);
            if (offsetX !== undefined && offsetY !== undefined)
                pattern = pattern.targetOffsetSync(offsetX, offsetY);
            return this.screen.findSync(pattern);
        } catch (error) {
            this.error = "find failed for pattern: " + filename + ' ' + error;
            return null
        }
    }
    findall(filename) {
        try {
            let all = this.screen.findAllSync(filename);
            return all;
        } catch (error) {
            return null;
        }
    }
    click(pattern) {
        try {
            this.screen.clickSync(pattern);
            return true;
        } catch (error) {
            return null;
        }
    }
    softClose(exeString, imageFile = null) {
        try {
            exec.execSync('Taskkill /IM ' + exeString + '.exe').toString();
            return true;

        } catch (error) {
            return false
        }


        // return "exec()";
    }
    hardClose(exeString) {
        try {
            const checkexplorer = this.runShellSync("tasklist /V /FO CSV /NH /FI \"IMAGENAME eq iexplore.exe\" /FI \"USERNAME eq %username%\" ");
            let explorer;

            if (checkexplorer.indexOf("No tasks are running") >= 0) {
                return true;
            } else {
                explorer = exec.execSync('Taskkill /F /IM ' + exeString + ' /FI \"USERNAME eq %username%\" ').toString();
                return true;
            }
        } catch (err) {
            return true;
        }

    }

    doubleClick(filename) {
        try {
            let click;
            if (filename) {

                click = this.screen.doubleClickSync(filename);
            }
            return click;
        } catch (error) {
            return null;
        }
    }

    CreateRect(x = null, y = null, h = null, w = null) {
        try {
            let rect = this.java.newInstanceSync("java.awt.Rectangle", x, y, h, w);
            this.CreateRegion(rect)
            return true
        } catch (error) {
            return null;
        }

    }
    CreateRegion(pattern) {
        try {

            return this.java.callStaticMethodSync("org.sikuli.script.Region", "create", pattern)

        } catch (error) {
            return null;
        }
    }
    clickImage(filename, similar, offsetX, offsetY) {
        try {
            let pattern = null;
            if (similar !== undefined)
                pattern = this.java.newInstanceSync("org.sikuli.script.Pattern", filename).similarSync(similar);
            else
                pattern = this.java.newInstanceSync("org.sikuli.script.Pattern", filename).similarSync(0.90);
            if (offsetX !== undefined && offsetY !== undefined)
                pattern = pattern.targetOffsetSync(offsetX, offsetY);
            this.screen.clickSync(pattern);

            return true;
        } catch (error) {

            this.error = "find failed for pattern: " + filename + ' ' + error;
            return null
        }
    }

    runShell(command) {
        try {
            return exec.exec(command).toString();
        } catch (error) {
            this.debugerror(error);
            return false;
        }
    }
    runShellSync(command) {
        try {
            return exec.execSync(command).toString();
        } catch (error) {
            this.debugerror(error);
            return false;
        }
    }
    appFocus(name) {
        let app;
        try {

            app = this.java.callStaticMethodSync("org.sikuli.script.App", "focus", name);
            // app.focus(name);
            return app;

        } catch (error) {

            return null;
        }
    }
    getApp(name) {
        return this.java.newInstanceSync("org.sikuli.script.App", name);

    }
    openApp(name) {
        try {
            const p = this.java.callStaticMethodSync("org.sikuli.script.App", "open", name);
            this.debug(p);

            return p
        } catch (error) {
            this.debugerror(error);
            return null;
        }
    }
    exists(target, timeout) {
        try {
            let match = null;
            if (timeout !== undefined)
                match = this.screen.existsSync(target, timeout);
            else
                match = this.screen.existsSync(target);
            if (config.settings.matchScore !== undefined) {
                return (match.getScore() > config.settings.matchScore);
            } else {
                return (match.getScore() > 0.90);
            }
        } catch (error) {
            return null;
        }
    }
    type(text, pattern, modifier) {
        try {
            let newtxt = typeof text ==='string'? text : ""+text+""
            if (pattern !== undefined) {
                if (modifier !== undefined) {
                    this.screen.typeSync(pattern, newtxt, modifier);
                } else {
                    this.screen.typeSync(pattern, newtxt);
                }
            } else {
                this.screen.typeSync(newtxt);
            }
            return true;
        } catch (error) {

            //debug(error);
            return null;
        }
    }

    capture(img) {
        try {
            let image = this.screen.captureSync();
            if (fs.existsSync(path.join(__dirname, "../../", config.soprafolder))) {
                fs.renameSync(image.getFilenameSync(), path.join(__dirname, "../../", config.soprafolder + "/" + img + ".png"))
            }
            return true;
        } catch (err) {
            return null;
        }
    }
    wait(pattern, timeout) {
        try {
            let wait
            if (pattern && timeout !== undefined)
                wait = this.screen.waitSync(pattern, timeout);
            else if (pattern && timeout === undefined)
                wait = this.screen.waitSync(pattern);
            else if (!pattern && timeout !== undefined)
                wait = this.screen.waitSync(timeout);
            return wait;
        } catch (error) {
            return false;
        }
    }
    parse(text) {
        this.script = text;
        return true;

    }
    execute() {
        //console.log('executing ' + this.script);
        return {
            status: "OK",
            data: {
                done: "there"
            }
        }
    }
    text(targetImage, message) {
        let newtxt = typeof message ==='string'? message : ""+message+""
        try {
            this.screen.pasteSync(targetImage, newtxt)
            return true;
        } catch (error) {
            return null
        }
    }
    keys(input, input2 = null) {
     //   console.log('executing ' + this.script);

        try {

            let Key = this.java.import("org.sikuli.script.Key");
            let KeyModifier = this.java.import("org.sikuli.script.KeyModifier");
            if (input2) this.screen.typeSync(Key.UP, KeyModifier.WIN);

            else this.screen.typeSync(input);

            return true;
        }
        catch (err) {

            return null;
        }
    }

    FindImageText(image, distance = null, similar = null) {
        try {

            let KeyModifier = this.java.import("org.sikuli.script.KeyModifier");
            let Env = this.java.import("org.sikuli.script.Env");
            this.click(image);
            for (let j = 0; j < 5; j++) {
                let clickvalue = this.doubleClick(image);
                if (clickvalue) break;
            }
            this.screen.typeSync("c", KeyModifier.KEY_CTRL);
            return Env.getClipboardSync()

        } catch (error) {
          //  console.log(error)
            return false
        }
    }
    highlight(pattern = null, imagePath = null, timeout = null) {

        try {
            let timeout_set = (timeout !== null) ? timeout : 4;
            if (pattern !== null) {
                pattern.highlightSync(timeout_set)
            }
            else {
                let screen_highlight = this.screen.findSync(imagePath);
                screen_highlight.highlightSync(timeout_set)
            }
            return true;

        } catch (error) {

            return null;
        }
    }
}
module.exports = ActionPath
