# consectus-rpa

Conspectus Robotics Process Automation using Sikuli, NodeJS &amp; node-java.

## Pre-installation

### JDK 8
Please download and install Java 8 SE not JRE and add the jdk location to the JAVA_HOME environment variable.
Also add jdk\bin to the path variable so that javac.exe can be reached.

### node-gyp
As an administrator, install following to global NPM location
```
npm install -g node-gyp
npm install --global --production windows-build-tools
```
After this installation PYTHON environment variable must be setup to python.exe file path.

### Sikulix Jar download with Jython Jar
To test the recordings using Sikulix IDE, please download from https://raiman.github.io/SikuliX1/downloads.html
SikuliX IDE (Jar file)
https://raiman.github.io/SikuliX1/sikulix.jar

They Jyton interpreter
https://repo1.maven.org/maven2/org/python/jython-standalone/2.7.1/jython-standalone-2.7.1.jar

Keep them in the same folder.
Double click the Sikulix.jar file to run it.

## SikuliX API Documentation
The URL below has the API Docs that outline how to use Screen, Pattern, Match, Region as Java API.
http://doc.sikuli.org/javadoc/index.html

## node-java bridge
This project uses the node-java bridge.
Each class in Sikulix needs to be instantiated and then methods on the call can be called.
By default all method calls are promise based but by adding "Sync" to the name of the method call, the call becomes blocking and returns when executed completely.
Sikulix uses exceptions to inform if the operation could not be completed.
For example, Screen.findSync("filename.png") would throw FindFailed exception meaning the patter in the file cannot be found.

When using static fields of a class or methods, please refer to node-java wrappers that allow us to use the fields & call methods.
